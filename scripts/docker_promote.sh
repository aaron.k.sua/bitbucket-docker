#!/bin/bash
#TODO Consider setting this as a local and pipeline environment variable
REGISTRY="lob-docker.jfrog.io"

if [[ "${ARTIFACTORY_USER}" != "" ]]
then
  docker login $REGISTRY -u $ARTIFACTORY_USER -p $ARTIFACTORY_PASSWORD
fi

CHANGES=( $(git log -m -1 --name-only --diff-filter=d --pretty="format:" | grep "dockerfiles/.*/\(Dockerfile\|scripts/.*\)" | sed -E -e "s/dockerfiles\/(.*)\/(Dockerfile|scripts\/.*)/\1/g" | sort -u) )
INDEXES=( $(git log -m -1 --name-only --diff-filter=d --pretty="format:" | grep "dockerfiles/.*/[[:digit:]]_.*/\(Dockerfile\|scripts/.*\)" | sed -E -e "s/dockerfiles\/.*\/([[:digit:]])_.*\/(Dockerfile|scripts\/.*)/\1/g" | sort -u) )
#echo ${INDEXES[@]}

#Find the lowest value of the indexes to properly chain the builds
INDEXMIN=${INDEXES[0]}
for i in "${INDEXES[@]}"; do
  (( i < INDEXMIN )) && INDEXMIN=$i
done

if [[ ${INDEXMIN} < 1 && ${INDEXMIN} != "" ]]
then
  echo "add java build"
  CHANGES+=("cipipeline/alpine/1_java")
fi
if [[ ${INDEXMIN} < 2 && ${INDEXMIN} != "" ]]
then
  echo "add base build"
  CHANGES+=("cipipeline/alpine/2_base")
fi
if [[ ${INDEXMIN} < 3 && ${INDEXMIN} != "" ]]
then
  echo "add aws and salesforce build"
  CHANGES+=("cipipeline/alpine/3_aws")
  CHANGES+=("cipipeline/alpine/3_salesforce")
fi

case ${BITBUCKET_BRANCH} in
  staging)
    ENV='stg'
    FROM='int'
    ;;
  production)
    ENV='prd'
    FROM='int'
    ;;
  *)
    exit 1
    ;;
esac

CHANGES=( $(printf '%s\n' "${CHANGES[@]}" | sort -u) )

set -e
for (( i=0; i<${#CHANGES[@]}; i++))
do 
    CHANGE=${CHANGES[i]}
    IMAGE_NAME=$(sed "s/\(.*\)[[:digit:]]_\(.*\)/\1\2/g" <<< "${CHANGES[$i]}")
    echo "Promoting image: $IMAGE_NAME"
    docker pull $REGISTRY/$IMAGE_NAME/${FROM}:latest
    docker tag $REGISTRY/$IMAGE_NAME/${FROM}:latest $REGISTRY/$IMAGE_NAME/${ENV}:latest
    export RAW_VERSION=$(docker run $REGISTRY/$IMAGE_NAME/${ENV}:latest --version)
    export VERSION=$(echo ${RAW_VERSION} | sed "s/[A-Z,a-z\s\ \/\_(=-]*\([0-9]*\.[0-9]*\.[0-9]*\).*/\1/g")
    echo "Found version: $VERSION"
    if [[ ${VERSION} == "" ]] 
    then
      export VERSION="latest_version"
    fi
    export MAJORVERSION=$(echo $VERSION | sed "s/\([0-9]*\)\.[0-9]*\.[0-9]*.*/\1/g")
    export MINORVERSION=$(echo $VERSION | sed "s/\([0-9]*\.[0-9]*\)\.[0-9]*.*/\1/g")
    export VERSION_BUILD="${VERSION}_${BITBUCKET_BUILD_NUMBER}"

    docker tag $REGISTRY/$IMAGE_NAME/prd:latest $REGISTRY/$IMAGE_NAME/prd:$VERSION
    docker tag $REGISTRY/$IMAGE_NAME/prd:latest $REGISTRY/$IMAGE_NAME/prd:$MAJORVERSION
    docker tag $REGISTRY/$IMAGE_NAME/prd:latest $REGISTRY/$IMAGE_NAME/prd:$MINORVERSION
    docker tag $REGISTRY/$IMAGE_NAME/prd:latest $REGISTRY/$IMAGE_NAME/prd:$VERSION_BUILD

    docker push $REGISTRY/$IMAGE_NAME/$ENV:latest
    docker push $REGISTRY/$IMAGE_NAME/$ENV:$VERSION
    docker push $REGISTRY/$IMAGE_NAME/$ENV:$MAJORVERSION
    docker push $REGISTRY/$IMAGE_NAME/$ENV:$MINORVERSION
    docker push $REGISTRY/$IMAGE_NAME/$ENV:$VERSION_BUILD

done