#!/bin/bash
pushd ${project_name}
  if [[ -d "${DIR_NAME}/scripts" ]]
  then
    FILE_LIST=( $(find ${DIR_NAME}/scripts/** -regex .*.sh) )
    echo ${FILE_LIST[@]}
    for (( filecount=0; filecount<${#FILE_LIST[@]}; filecount++))
    do
      echo "editing ${FILE_LIST[filecount]}"
      sed -i "2iecho \"this file is located at ${CI_PROJECT_URL}/-/tree/integration/${FILE_LIST[filecount]}\"" ${FILE_LIST[filecount]}
    done
  fi
popd