# Overview
The scripts here make it easier to launch docker containers from local host operating systems.


# Prereqs
## Add authentication with Artifactory for Local Docker Commands

Login to Artifactory. https://lob.jfrog.io/lob/webapp/#/home - this should use your Active Directory user for SSO.
Inside of Artifactory, navigate to your user profile by clicking your username in the top right.
In the User Profile screen, click "Generate API Key". If you already have generated an API key, copy the API key to your clipboard.
 Inside of a command prompt window, execute the following command: 

```
docker login lob-docker.jfrog.io
```
Provide your user name as your email address.
Provide your API key as your password.
Test by pulling the base nodejs image with the following command:

```
docker pull lob-docker.jfrog.io/cipipeline/alpine/node/prd:latest
```

## Clone the ci-docker repo and add the scripts/ directory to ~/bin
```
% git clone -b production git@bitbucket.org:liveoakbank/ci-docker.git
% ln -s $(pwd)/ci-docker/scripts/ ~/bin/
```

Edit ~/.profile or ~/.zshrc for ZShell or ~/.bash_profile for Bash
```
export PATH=~/bin/scripts:~/bin:/usr/local/bin:/usr/local/sbin:$PATH
```

## Authenticate with Artifactory for local docker containers
startup the aws deployer container with
```
deployer-aws
```
then execute the same login commands as in the previous step:
```
docker login lob-docker.jfrog.io
```
Provide your user name as your email address.
Provide your API key as your password.

## Setup Octopus
 - [Configure local to work with Octopus](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/772505786/Configure+local+to+work+with+Octopus)

# Script Details

## awsConsole
used to open the browser with an assumed role to a particular environment AKA a profile.
Supports several methods for managing credentials used to connect to AWS.
The native AWS Profile Configuration, Profiles as .env files, and the established Environment Variables

- The native AWS CLI and CDK profiles typically stored in ${HOME}/.aws
There are two files ${HOME}/.aws/config and ${HOME}/.aws/credentials
[AWS Documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)

example:

config:

```
[developer]
aws_account = <your aws account>
output = json
region = us-east-1

[integration]
aws_account = <integration aws account>
role_arn = AWSReadAccessRole
output = json
region = us-east-1

[staging]
aws_account = <staging aws account>
role_arn = AWSReadAccessRole
output = json
region = us-east-1

[production]
aws_account = <production aws account>
role_arn = CloudWatchLogReadAccessRole
output = json
region = us-east-1
```

credentials:

```
[developer_creds]
aws_access_key_id = <your aws access key>
aws_secret_access_key = <your aws secret>

[integration]
role_arn = arn:aws:iam::<integration account>:role/AWSReadAccessRole
source_profile = developer_creds

[staging]
role_arn = arn:aws:iam::<staging account>:role/AWSReadAccessRole
source_profile = developer_creds

[production]
role_arn = arn:aws:iam::<production account>:role/CloudWatchLogReadAccessRole
source_profile = developer_creds
```

- The .env files housed in ${HOME}/.aws_profiles/.  Each Profile/Environment is a separate file

example:

developer.env

```
AWS_ACCOUNT=<your aws account>
AWS_ACCESS_KEY_ID=<your aws access key>
AWS_SECRET_ACCESS_KEY=<your aws secret>
AWS_ROLE=Developer
AWS_DEFAULT_REGION=${$AWS_REGION:-us-east-1}
```

integration.env

```
AWS_ACCOUNT=<integration account>
AWS_ACCESS_KEY_ID=<your aws access key>
AWS_SECRET_ACCESS_KEY=<your aws secret>
AWS_ROLE=AWSReadAccessRole
AWS_DEFAULT_REGION=us-east-1
```

- As well as whatever ENV VARS are set prior to executing the commands.


```
-p for PROFILE, The profile or environment to use, do not include .env if using aws_profiles/ method
                   for the aws script if you specify CONFIG you must also specify the PROFILE to use.
-c for CONFIG, the path to the standard AWS profiles config file
-s for STAGE, the stage of container to utilze, typically 'dev', 'int', or 'prd'
```

## deployer-aws
1 positional argument for the STAGE of the container.  Provided as 'alpha' for 'dev', 'beta' for 'int', and null for 'prd'.
legacy container for running pipelines.  Currently deprecated in favor of pipelineBash

## deployer-sfdx
1 positional argument for the STAGE of the container.  Provided as 'alpha' for 'dev', 'beta' for 'int', and null for 'prd'.
legacy container for running pipelines.  Currently deprecated in favor of pipelineBash

## octodeploy script
1 positional argument for the STAGE of the container.  Provided as 'alpha' for 'dev', 'beta' for 'int', and null for 'prd'.
executed from your repo to build and deploy your project.
- see [How to deploy using Octopus](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/772898939/How+to+deploy+using+Octopus)

## pipelineBash
1 positional argument for the STAGE of the container.  Provided as 'alpha' for 'dev', 'beta' for 'int', and null for 'prd'.
This gives you an interactive terminal session in the same base container used in CI.  All the scripts and utilities are able to be run from this privleged container.  To ensure the most accurate simulation of pipeline execution it is recommended to use this script locally as well as the same image in your pipeline file.

On instantiation the container will have the root of the repo you are currently in, and set the PWD to match your current PWD

This script also supports the profile controls like awsConsole.  This is primarily to support debug and diagnostics between users.

# Scripts for use in this repo

## docker_build.sh
For legacy builds the script utilizes a layered approach to improve build and execution times for larger containers, these dockerfiles are housed in dockerfiles/cipipeline.

For current builds the dockerfiles/devtools and dockerfiles/langs directories house specific utilities to be leveraged as smaller separate containers.

THe script leverages git to determine what images to build in this monorepo.

When not executed from CI and the Integration branch it does not push the builds to the registry.

All .sh scripts have an echo appended to the top of the file to announce the repo and a direct link to where the file source is kept.

Docker images are also tagged with semantic versioning for historical storage and reference

### Chained image builds
The images in cipipeline have numbered directories to chain the builds.  The lowest number, 0_, is the base image build, and that is consumed by other dockerfiles to build subsequent images.  This creates images with layers in common and improves build times and downloads, as well as offering some ease of maintenance with common dependencies and scripts.

This means that the `scripts/` found in `0_node/` will also be in the resulting image built for `1_java/`, `2_base/`, `3_aws/`, and `3_salesforce/`.

The docker_build.sh and docker_promote.sh scripts take this relationship into account when determining what images to build.  So if `0_node` is rebuilt, so are all the other images that have it as a dependency.

## docker_promote.sh
Utilizes git to determine what images to promote from 'int' name space to 'prd'