#!/bin/bash
#TODO Consider setting this as a local and pipeline environment variable
REGISTRY="lob-docker.jfrog.io"

if [[ "${ARTIFACTORY_USER}" != "" ]]
then
    docker login $REGISTRY -u $ARTIFACTORY_USER -p $ARTIFACTORY_PASSWORD
fi

#Get a list of directories to build docker images for and store as an array.
#git log outputs a list of files added or modified, but not deleted
#use grep to only find the files that are in the dockerfiles/ directory
#use sed to get the top directory that houses the Dockerfile and the index order for multi layer builds
CHANGES=( $(git log -m -1 --name-only --diff-filter=d --pretty="format:" | grep "dockerfiles/.*/\(Dockerfile\|docker-entrypoint\.sh\|scripts/.*\)" | sed -E -e "s/dockerfiles\/(.*)\/(Dockerfile|docker-entrypoint\.sh|scripts\/.*)/\1/g" | sort -u) )
INDEXES=( $(git log -m -1 --name-only --diff-filter=d --pretty="format:" | grep "dockerfiles/cipipeline/.*/\(Dockerfile\|docker-entrypoint\.sh\|scripts/.*\)" | sed -E -e "s/dockerfiles\/.*\/([[:digit:]])_.*\/(Dockerfile|docker-entrypoint\.sh|scripts\/.*)/\1/g" | sort -u) )
#echo ${INDEXES[@]}

INDEXMIN=${INDEXES[0]}
for (( i=0; i<${#INDEXES[@]}; i++ ))
do
  (( i < INDEXMIN )) && INDEXMIN=${INDEXES[$i]}
done
#echo $INDEXMIN

if [[ ${INDEXMIN} < 1 && ${INDEXMIN} != "" ]]
then
  echo "add java build"
  CHANGES+=("cipipeline/alpine/1_java")
fi
if [[ ${INDEXMIN} < 2 && ${INDEXMIN} != "" ]]
then
  echo "add base build"
  CHANGES+=("cipipeline/alpine/2_base")
fi
if [[ ${INDEXMIN} < 3 && ${INDEXMIN} != "" ]]
then
  echo "add aws and salesforce build"
  CHANGES+=("cipipeline/alpine/3_aws")
  CHANGES+=("cipipeline/alpine/3_salesforce")
fi

CHANGES=( $(printf '%s\n' "${CHANGES[@]}" | sort -u) )

case ${BITBUCKET_BRANCH} in
  integration)
    ENV='int'
    ;;
  staging)
    ENV='stg'
    ;;
  production)
    ENV='prd'
    ;;
  *)
    ENV='dev'
    ;;
esac

if [[ ${CHANGES[0]} != "" ]]
then
    echo "Changes are: ${CHANGES[@]}"
    pushd dockerfiles
    set -e
    for (( i=0; i<${#CHANGES[@]}; i++))
    do 
        DIR_NAME="${CHANGES[$i]}"
        IMAGE_NAME=$(sed "s/\(.*\)[[:digit:]]_\(.*\)/\1\2/g" <<< "${CHANGES[$i]}")
        echo "Building image: ${IMAGE_NAME}"
        if [[ -d "${DIR_NAME}/scripts" ]]
        then
          #go through each script and add an echo line at the top
          #FILE_LIST=( $(ls $DIR_NAME/scripts/ | grep -v ".*\.py") )
          FILE_LIST=( $(find ${DIR_NAME}/scripts/** -regex .*.sh) )
          echo ${FILE_LIST[@]}
          for (( filecount=0; filecount<${#FILE_LIST[@]}; filecount++))
          do
            echo "editing ${FILE_LIST[filecount]}"
            sed -i "2iecho \"this file is located at ${BITBUCKET_GIT_HTTP_ORIGIN}/src/production/dockerfiles/${FILE_LIST[filecount]}\"" ${FILE_LIST[filecount]}
          done
        fi
        pushd ${DIR_NAME} 
          docker build ./ -t ${REGISTRY}/${IMAGE_NAME}/${ENV}:latest
          if [[ ${ENV} != "dev" ]]
          then
              docker push ${REGISTRY}/${IMAGE_NAME}/${ENV}:latest 
          fi
        popd
    done
    popd
else
    echo "No files changed to result in any builds."
fi