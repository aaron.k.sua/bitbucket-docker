#!/bin/bash
set -e

echo "#{ARTIFACTORY_PASSWORD}" | docker login #{ARTIFACTORY_REGISTRY} -u #{ARTIFACTORY_USERNAME} --password-stdin
REGISTRY="#{ARTIFACTORY_REGISTRY}"
DOCKER_IMAGE="${REGISTRY}/${DOCKER_NAMESPACE:-"devtools"}/bash/prd:latest"

docker pull ${DOCKER_IMAGE}

if [[ -f "CDKPackage/octopusDeploy.env" ]]
then
  echo "Found ENV file"
  ENV_FILE="--env-file CDKPackage/octopusDeploy.env"
else
  echo "No ENV file"
  ENV_FILE=""
fi

BASE_PATH="/opt/atlassian/pipelines/agent/build"
echo "Setup complete, Running"

COMMAND="docker run --rm -i --net lob --privileged \
-v /var/run/docker.sock:/var/run/docker.sock ${ENV_FILE} \
--env-file <(env | sed \"s/^PATH=.*//g\" | sed \"s/^HOME=.*//g\") \
-e AWS_ACCOUNT=#{AWS_ACCOUNT} \
-e AWS_ROLE=#{AWS_ROLE} \
-e AWS_DEFAULT_REGION=#{AWS_REGION}
-e AWS_REGION=#{AWS_REGION} \
-e EnvironmentName=#{StageName} \
-e ARTIFACTORY_USER=#{ARTIFACTORY_USERNAME} \
-e ARTIFACTORY_PASSWORD=#{ARTIFACTORY_PASSWORD} \
-e ARTIFACTORY_URL=\"#{ARTIFACTORY_URL}\" \
-e BASE_PATH=${BASE_PATH} \
-e REPO_PATH=${PWD}/ \
-e BITBUCKET_CLONE_DIR=${BASE_PATH} \
-v ${HOME}/.docker:/root/.docker \
-v ${PWD}:${BASE_PATH}/ \
-w ${BASE_PATH}/CDKPackage \
--name #{Octopus.Project.Name} \
--entrypoint /bin/bash \
${DOCKER_IMAGE} -c \"deployCDK.sh #{CDK_COMMAND}\""

echo ${COMMAND}
eval ${COMMAND}