#!/bin/bash
echo "Starting Setup"
echo "#{ARTIFACTORY_PASSWORD}" | docker login #{ARTIFACTORY_REGISTRY} -u #{ARTIFACTORY_USERNAME} --password-stdin
DOCKER_NETWORKS=( $(docker network ls -q -f name=lob) )
if [[ ${#DOCKER_NETWORKS[@]} == 0 ]]
then
  docker network create lob
fi

STAGE="#{STAGE}"
REGISTRY="#{ARTIFACTORY_REGISTRY}"
DOCKER_IMAGE="${REGISTRY}/${DOCKER_NAMESPACE:-"devtools"}/bash/${STAGE}:latest"

docker pull ${DOCKER_IMAGE} > /dev/null

if [[ -f "AnsiblePackage/octopusDeploy.env" ]]
then
  echo "ENV file found"
  ENV_FILE="--env-file AnsiblePackage/octopusDeploy.env"
else
  echo "No ENV file"
  ENV_FILE=""
fi

MY_PWD=${PWD#"${REPO_PATH:-$PWD}"}
BASE_PATH="/opt/atlassian/pipelines/agent/build"
echo "Setup complete, Running"

COMMAND="docker run --rm -i --net lob --privileged \
-v /var/run/docker.sock:/var/run/docker.sock ${ENV_FILE} \
--env-file <(env | sed \"s/^PATH=.*//g\" | sed \"s/^HOME=.*//g\") \
-e AWS_ACCOUNT=#{AWS_ACCOUNT} \
-e AWS_ROLE=#{AWS_ROLE} \
-e AWS_DEFAULT_REGION=#{AWS_REGION}
-e AWS_REGION=#{AWS_REGION} \
-v \"${HOME}/.ssh/known_hosts:/root/.ssh/known_hosts\" \
-v \"${HOME}/.gitconfig:/root/.gitconfig\" \
-v \"${HOME}/.docker:/root/.docker\" \
-v \"${HOME}/.ssh/:/root/.ssh/\" \
-v \"${HOME}/.aws/:/root/.aws/\" \
-v \"${HOME}/.aws_profiles/:/root/.aws_profiles/\" \
-v \"${REPO_PATH:-$PWD}:${BASE_PATH}/\" \
-w ${BASE_PATH}/AnsiblePackage \
-e BASE_PATH=\"${BASE_PATH}\" \
-e REPO_PATH=${REPO_PATH:-$PWD}/ \
-e HOST_HOME=${HOME} \
-e STAGE=${STAGE} \
-e DYNAMODB=dynamodb \
--name #{Octopus.Project.Name} \
${DOCKER_IMAGE} ansible #{ANSIBLE_COMMAND}"

echo ${COMMAND}

eval ${COMMAND}