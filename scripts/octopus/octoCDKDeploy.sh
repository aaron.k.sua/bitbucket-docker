#!/bin/bash
set -e

echo "#{ARTIFACTORY_PASSWORD}" | docker login #{ARTIFACTORY_REGISTRY} -u #{ARTIFACTORY_USERNAME} --password-stdin
REGISTRY="#{ARTIFACTORY_REGISTRY}"
DOCKER_IMAGE="${REGISTRY}/${DOCKER_NAMESPACE:-"langs"}/node/prd:latest"

docker pull ${DOCKER_IMAGE}

if [[ -f "octopusDeploy.env" ]]
then
  ENV_FILE="--env-file octopusDeploy.env"
else
  ENV_FILE=""
fi

docker run --rm \
-t $(tty &>/dev/null && echo "-i") ${ENV_FILE} \
-e AWS_ACCOUNT=#{AWS_ACCOUNT} \
-e AWS_ROLE=#{AWS_ROLE} \
-e AWS_ACCESS_KEY_ID=#{AWS_ACCESS_KEY_ID} \
-e AWS_SECRET_ACCESS_KEY=#{AWS_SECRET_ACCESS_KEY} \
-e AWS_REGION=#{AWS_REGION} \
-v ${PWD}:${PWD} \
-w ${PWD}/CDKPackage \
--entrypoint assumeRole.sh \
${DOCKER_IMAGE} "npx cdk #{CDK_COMMAND}"
