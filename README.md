For general Documentation on Docker please see [here](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/761888875/Dockermentation).


# Repo Mission:
## Contract
### Devtools will:

  - Ensure unique versions are published to the registry
  - Maintain and review the dockerfiles that define various tooling for use in pipeline execution
  - Maintain and review a working default set of scripts and pipeline example for project types (language and deployment)
  - Maintain and review a working set of scripts or tools to execute pipeline containers on developer's local machines

### Devtools would like to suggest that developers:

  - If this is a node js project, have a naming convention in package.json for certain scripts.  lint, test, and build.
    - currently prebuild_test.sh has dependencies on lint and test npm scripts.
    - build scripts should create artifacts of `package.zip`, `deployable.zip`, or both in order to use the default deploy.sh
  - The default scripts for `set_ssh.sh`, `set_npmrc.sh`. `set_dockerrc.sh`, `rev_version.sh`, `promote.sh`, `deploy.sh`, and `postdeploy.sh` not be customized without guidance from DevTools Team as these scripts have particular interactions with each other and the Octopus Deployment Tool for managing consistent deployments.
  - If the projects npm scripts are complicated or should be easily shared, make them bash shell scripts.
  - If a more complex language than bash is needed, consider building a separate tool and artifact.

## Purpose
  - To provide consistent runtime environments for the SDLC from local workstation to production
  - Provide working examples and shareable code for use locally and in bitbucket pipeline

# Repository Layout
```
dockerfiles/      The dockerfiles and associated scripts for the various tools used in executing tests, builds, and deployments.
  cipipeline      [Deprecated] The dockerfiles and scripts for the base docker image used in the pipeline file
    0_node        The base image that is consumed by the other builds.  Contains scripts and tools for basic node module publication
    1_java        Includes 0_node and installs java.  This image is used by projects building an SDK from a swagger definition.
    2_base        Includes 1_java and 0_node.  Not currently used by any projects but includes common tools needed by 3_aws and 3_salesforce
    3_aws         Includes 2_base, 1_java, and 0_node.  Used by both gradle and octopus style deployments of AWS lambdas and cloud formation
    3_salesforce  Includes 2_base, 1_java, and 0_node.  Used by salesforce code push and sfdx style deployments
  devtools        The dockerfiles and scripts for various other tools
    ansible         A python based infrastructure as code automation tool / language
    aws_cli         The scripts for logging in and executing shell commands with an assumed role
    bash            The new base image for pipeline execution
    cypress         WebUI testing framework
    octopus         The scripts for createANDDeployrelease as well as promoterelease.  Used locally and by pipeline for octopus deployments
    pact_broker     A testing utility for managing contract based testing
    semversioner    Utility to help manage semantic versioning in the repos
    sfdx            Salesforce command line utility
  langs             Programming language specific containers
    node
    python
    java
scripts/          The scripts for executing tasks through containers locally on a bash shell and manage this repo
  docker_build.sh    The script used by this repo to build the docker images
  docker_promote.sh  The script used by this repo to publish the docker images
```

Many of the docker images built here have a folder in the path: `/opt/pipeline/scripts/` that contains a default set of scripts for use in a pipeline execution.  For each docker image the associated script files can be found next to the dockerfile in a `scripts/` directory.

These scripts are intended to be a common, default example of shareable pipeline steps.

Consult their READMEs for additional informaiton

# Setup Local (OSx) for ci-docker scripts
For this repo, and all others it is highly recommended to use docker containers to execute builds and tests for consistency, stability and compatability.
Follow these instructions to ensure your host is properly configured to instantiate the containers.

## Prerequisites

### Install Docker
Install docker for your given host.

NOTE:

  - For OSx do not attempt to install docker via brew, you must utilize the installer from the Docker website.

## Clone the ci-docker repo and add the scripts/ directory to ~/bin
```
% git clone -b production git@bitbucket.org:liveoakbank/ci-docker.git
% mkdir -p ~/bin/
% ln -s $(pwd)/ci-docker/scripts/ ~/bin/
```

Edit ~/.profile or ~/.zshrc for ZShell or ~/.bash_profile for Bash
```
export PATH=~/bin/scripts:~/bin:/usr/local/bin:/usr/local/sbin:$PATH
```

## Authentication with Artifactory for OSx Docker Commands

Login to Artifactory. https://lob.jfrog.io/lob/webapp/#/home - this should use your Active Directory user for SSO.
Inside of Artifactory, navigate to your user profile by clicking your username in the top right.
In the User Profile screen, click "Generate API Key". If you already have generated an API key, copy the API key to your clipboard.
 Inside of a command prompt window, execute the following command: 

```
docker login lob-docker.jfrog.io
```
Provide your user name as your email address.
Provide your API key as your password.
Test by pulling the base nodejs image with the following command:

```
docker pull lob-docker.jfrog.io/cipipeline/alpine/node/prd:latest
```

## Authenticate with Artifactory for local docker containers running Docker Commands
startup the pipeline container with
```
pipelineBash
```
then execute the same login commands as in the previous step:
```
docker login lob-docker.jfrog.io
```
Provide your user name as your email address.
Provide your API key as your password.

NOTE:

  - This step is referencing a technique commonly referred to as Docker In Docker.  OSx stores credentials differently than Linux, making this step necessary to ensure push and pull commands to the Artifactory Docker Registry succeed.
  - In order for network discovery to operate properly between multilpe containers you need to establish and utilize a common network.  By executing the pipelineBash script the following command is executed for you.
```
docker network create lob
```

# Windows Support
Install wsl2 and ubuntu 20.04 or higher.

```
sudo apt-get update
sudo apt-get install dos2unix
mkdir ~/bin
mkdir /mnt/c/Users/<your user>/.aws_profiles/
ln -s /mnt/c/<path to ci-docker>/ci-docker/scripts ~/bin/scripts
ln -s /mnt/c/Users/<your user>/.aws_profiles/ ~/.aws_profiles
echo 'export PATH="~/bin/scripts:${PATH}"' >> ~/.bashrc
source ~/.bashrc
```

# Using these tools locally
Start the `pipelineBash` container to run any command as if it were being run in Bitbucket Pipeline context, or pass a command/script directly to it.

examples:

To build and deploy your project, from within the repo execute:
```
MacBook-Pro$ pipelineBash buildAndDeploy.sh
```

To execute a node script:
```
MacBook-Pro$ pipelineBash node path/to/my/script.js
```

Leaving the pipelineBash container up for multiple commands:
```
MacBook-Pro$ pipelineBash
bash-5.0# buildNPMPack.sh
bash-5.0# deploy.sh
```

# Using these tools in Bitbucket Pipeline

## Instructions
From the example bitbucket-pipelines-*-example.yml files in this repo:
```
image:
    name: lob-docker.jfrog.io/devtools/bash/prd:latest
    username: $ARTIFACTORY_USER
    password: $ARTIFACTORY_PASSWORD
```
The image: block specifies the name of the docker image to use.  The Environment Variables for the credentials are used to login to Artifactory with a service user specifically for executing pipelines.

## Overriding the default scripts:

If you want to override the use of the scripts without making changes to the pipeline file itself you may create a scripts/ directory in the root of your repo and place the same named scripts there.  Ensure they have permissions for any user to execute to ensure that they will run in the pipeline.  For example: chmod 777 scripts/build.sh

## Deploying your projects and managing Versions
Deployments are done by utilizing a minimum of bash, octopus, and semversioner containers/images.

  - bash is the orchestrating container and the start of all executions
  - semversioner is the standard tool for tracking versions and changes directly in the repo
  - octopus takes all zip packages and applies version number before uploading to the octopus server for deployment

## Pipeline Flow for a Node JavaScript project:

Example of use case for using all default scripts from the docker container in the bitbucket pipeline.
![basic example](https://jensyn.atlassian.net/wiki/download/thumbnails/772505666/pipelineBasicFlow.png?api=v2&width=1051&height=487)

 


Example of a use case for using some default scripts and some scripts in the repositories /scripts directory.
![advanced example](https://jensyn.atlassian.net/wiki/download/thumbnails/772505666/piplelineAdvancedFlow.png?api=v2&width=980&height=500)

# Bitbucket Pipeline example(s) using ci-docker images/scripts
Create a base bitbucket-pipelines.yml file in the root of your repo similar to one of the examples in this repo:
```
bitbucket-pipelines-3-flow-example.yml          bitbucket-pipelines-trunk-flow-example.yml      bitbucket-pipelines.yml
```

In order to interact with git and artifactory there are some credential setting scripts:
```
set_ssh.sh      set_npmrc.sh         set_dockerrc.sh
```

take care with these credentials when building docker images and use a multi-stage build approach.

Scripts called in the same step will share the running state of the container, each new step definition in the pipeline will destroy the current container and use a brand new container and fresh clone of the repo.

Using artifacts definitions allows you to pass specific `file system state` changes between steps.  Good examples are build artifacts or node_modules/ directories.

Cache definitions store certain downloads for faster step execution and reduced data throughput usage.  By leveraging docker caches the associated images will not be downloaded automatically, and unchanged layers will not have to be downloaded again, saving time and bandwidth.

NOTE: 

 - Bitbucket pipeline does not properly manage docker users and permissions on artifacts and caches.  To use them you'll have to chmod 777 on all files/directories.

For additional details on configuring Pipelines, refer to [Bitbucket Pipeline documentation.](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html)

# Contributing Guidelines

## Contributing
The most common language to extend functionality in the containers is through shell and bash scripting.  The bash interpreter is very small and most needs can be fullfilled in the same manner you would run commands from your local terminal.

The majority of bash scripts in this repo began life as a series of bash commands being executed from the bitbucket pipeline file.  In order to share these common sections of bash commands between branches and repos those commands were packaged into shell scripts.

It is recommended that if you need a higher level language like nodejs or python to accomplish tasks that you create a package and store that in artifactory.  The package can be installed into a container/image and versioned as other tools have been, but for node you can also leverage the binary command npx.

### NPX
when npx is used to call a node module if it is already installed, globally or locally, it will execute that module as you would expect, however if it is not installed it will install it.  This affords some flexibility in execution and controlling what your pipeline is running.

### Dockerfile
This is the core instruction set for building images. Note the directory structure that every Dockefile is in a separate directory and sometimes accompanied by a scripts directory.  We've also added to the $PATH variable in several cases to handle the capability of over rides.  This directory structure keeps the build commands running quickly, and having few RUN commands in the Dockerfiles also helps with this.  Including the docker cache in pipeline furthers the performance gains where commands that are not changed do not get rebuilt.  So be very mindful of where you introduce changes as it determines what gets built.

## Testing
Always start by executing the pipelineBash script to start the bash container from within the repo you are working with.

Test script changes in a consuming repo by leveraging the over rides first.  Once you're sure it's working for the consuming repo it can be merged into the docker images to be shared with other consuming repos.

Test build changes and the resulting docker images by running the scripts/docker_build.sh, do note that this will only build based on the changes in your branch HEAD commit.  To ensure a complete build test of your whole branch, create a new branch from the HEAD of `integration` and call it build_test, don't push it to the remote.  Do a squash merge from your branch into build_test, then run the build script.

Test built image.  Running `pipelineBash alpha` will use /dev:latest namespaced images.  Take care that other images used in the commands you execute are also expected to be on your local host with the /dev:latest name space.  You can always pull the latest /prd:latest or /int:latest and tag them for use as needed.

NOTE: 

  - Do not push dev images to the registry as it may disrupt other developers working with locally built images.

### Local Functional Test
Create an over ride script for your repo in a scripts/ directory.  Then run the script from the root of your repo.
in pipeline the root of the repo is always `/opt/atlassian/pipelines/agent/build/`.  Verify the outcome is as expected.

### Local Docker Build Test
Ensure all of your changes are in the commit hash you are currently on.
from the root of this repo execute ./scripts/docker_build.sh

When running locally the namespace will have /dev:latest on the end and are usable by passing the `alpha` positional argument:
ex:
```
pipelineBash alpha
octodeploy alpha
```

This environment variable is then passed to all subsequent containers if applicable.

## Branch Management
For individual work please utilize a feature/ or bugfix branch name.
Always branch from the HEAD of Integration.

When merging your PR to Integration always `squash` and `close` the branch on bitbucket and delete the branch locally.  Reusing branches and squash can cause merge conflicts.  Merging to this branch will build and push to Artifactory into a namespace that includes /int:latest.  These images are usable in pipeline and local

When merging your PR from Integration to Production always use `merge` and not `squash`.  Merging to this branch will not rebuild the images, it takes the /int:latest tag  and retags them as /prd:latest as well as the relevant version numbers.