#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/publish.sh, please change your code"
cd $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u)
echo $(pwd)
set -e
npm version patch -m "Bumping patch version [skip CI]"
git push origin --follow-tags
npm publish --registry "https://lob.jfrog.io/lob/api/npm/npm/"