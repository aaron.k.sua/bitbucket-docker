#!/bin/sh
# Variables section
echo "WARNING WARNING WARNING This script is being deprecated and has no replacement script please change your code"
FORK_URL=$(echo $BITBUCKET_GIT_HTTP_ORIGIN | sed -e 's/'"$BITBUCKET_REPO_SLUG"'/starter-api/g')

echo "checkout $BITBUCKET_BRANCH"
git checkout "$BITBUCKET_BRANCH"
if [ "$BITBUCKET_BRANCH" =  "master" ]; then
	echo "add starter as upstream ..."
	git remote add upstream $FORK_URL
	echo "fetch upstream ..."
	git fetch upstream
	echo "merge upstream master..."
	git merge --no-edit upstream/master -m "Merged updates into $BITBUCKET_REPO_SLUG from its upstream repository, $FORK_URL."
	echo "Pushing to $BITBUCKET_REPO_SLUG branch $BITBUCKET_BRANCH"
	git push origin "$BITBUCKET_BRANCH"
fi