#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/installNodeDependencies.sh, please change your code"
echo "Install Node Dependencies"
#Find package.json files
PACKAGES=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
echo ${PACKAGES[@]}
len=${#PACKAGES}
set -e
for (( i=0; i<$len; i++))
do 
    if [[ ${PACKAGES[$i]} != "" ]]
    then
        echo "Iteration $i"
        pushd "${PACKAGES[$i]}"
        if [[ "${ARTIFACTORY_USER}" != "" ]]
        then
            jfrog rt config --url=$ARTIFACTORY_URL --user=$ARTIFACTORY_USER --password=$ARTIFACTORY_PASSWORD --interactive=false
            jfrog rt npm-install npm
        else
            npm install
        fi
        popd
    fi
done