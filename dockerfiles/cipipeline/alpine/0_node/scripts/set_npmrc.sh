#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/set_npmrc.sh, please change your code"
if [[ "${ARTIFACTORY_USER}" != "" ]]
then
    echo 'Configuring LOB NPM repository'
    NPMRC=${HOME}/.npmrc
    UIDPWD=$(echo -n "$ARTIFACTORY_USER:$ARTIFACTORY_PASSWORD" | openssl enc -base64)
    echo _auth = $UIDPWD > $NPMRC
    echo always-auth = true >> $NPMRC
    echo registry = ${ARTIFACTORY_URL}api/npm/npm/ >> $NPMRC
fi