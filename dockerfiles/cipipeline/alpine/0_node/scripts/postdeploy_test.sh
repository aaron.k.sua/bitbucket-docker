#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/postdeploy_test.sh, please change your code"
echo "Tests run after the artifact has been deployed"
cd $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u)
echo $(pwd)