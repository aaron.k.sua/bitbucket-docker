#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/ for building, deploying and promoting, please change your code"
echo "Build a New Artifact or Promote existing Artifact"
  BUILD_COMMAND="build.sh"
if [[ $1 == "null" ]]
then
  echo "WARNING no build command specified, defaulting to ${BUILD_COMMAND}"
else
  BUILD_COMMAND=$1
fi

set -e
#use git commands to determine if commit is a merge coming from the Integration branch
git fetch origin integration
INT_DIFF=$(git diff --raw HEAD..FETCH_HEAD)

if [[ ${INT_DIFF} == "" ]]
then
  echo "No diff, promote Integration build to Staging"
  promote.sh
else
  echo "Diff detected, build a new Artifact"
  ${BUILD_COMMAND}
  postbuild_test.sh
fi