#!/bin/sh
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/set_ssh.sh, please change your code"
set -e
echo $SSH_KEY > $HOME/.ssh/id_rsa.tmp
base64 -d $HOME/.ssh/id_rsa.tmp > $HOME/.ssh/id_rsa
chmod 600 $HOME/.ssh/*