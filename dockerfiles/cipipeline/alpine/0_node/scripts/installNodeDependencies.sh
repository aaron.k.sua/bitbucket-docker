#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/installNodeDependencies.sh, please change your code"
echo "Install Node Dependencies"
#Find package.json files
PACKAGES=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
echo ${PACKAGES[@]}
set -e

COUNT=${#PACKAGES[@]}
for (( i=0; i<${COUNT}; i++ ))
do
    PACKAGE=${PACKAGES[$i]}
    echo "Building ${PACKAGE}"
    pushd "${PACKAGE}"
        # $1 allows the user to pass --production or other command switches.  no error handling is currently offered around this functionality
        if [[ -z ${CI} ]]
        then
            echo "Running locally"
            npm install $1
        else
            echo "Running in CI"
            npm ci $1
        fi
    popd
done