#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/prebuild_test.sh, please change your code"
echo "Tests run on static source before the Artifact is built"
cd $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u)
echo $(pwd)
set -e
npm run lint
npm run test