#! /bin/bash
#
# Dependencies:
#   brew install jq
#
# Setup:
#   chmod +x ./assumeRole.sh
#
# Execute:
#   assumeRole.sh cdk --synth
#   - OR -
#   source assumeRole.sh
#
# Description:
#   Assume an AWS role prior to executing a command/script
#
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/tools/assumeRole, please change your code"
unset  AWS_SESSION_TOKEN
if [[ -z $AWS_REGION ]]; then
    export AWS_REGION=us-east-1
fi

temp_role=$(aws sts assume-role --role-arn arn:aws:iam::$AWS_ACCOUNT:role/$AWS_ROLE --role-session-name finxact-consumer-developer) 
echo
echo $temp_role
echo

export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)

env | grep -i AWS_

eval "${@}"
