#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/ for building, deploying and promoting, please change your code"
#This script can be used for deploying using existing gradle functionality or from Octopus.
#
# For Octopus deployments, the Octopus project name needs to be passed in
# $1 = Octopus project name.  Ideally, this will be the name of the repo.
#
# When deploying to Octopus, this script expects the following environment variables to be set/available:
# OctopusAWSAccount
# OctopusServer
# OctopusAPIKey
# AWS_ACCOUNT

echo "Deploy the Artifact to the proper environment, or publish a common component Artifact for others"
#for gradle deployments, find the project directory
set -e
PROJECTDIR=( $(find ** -maxdepth 1 -name build.gradle | sed -E -e "s/(.*)build.gradle/\.\/\1/g" | sort -u) )

if [[ -z $PROJECTDIR ]] #if build.gradle is not detected, deploy using octopus
then
    REGISTRY="lob-docker.jfrog.io"

    if [[ ${ARTIFACTORY_USER} != "" ]]
    then
        docker login $REGISTRY -u $ARTIFACTORY_USER -p $ARTIFACTORY_PASSWORD
    fi
    PROJECTNAME=${BITBUCKET_REPO_SLUG:-$1}
    if [[ -z $PROJECTNAME ]]; then echo "\$BITBUCKET_REPO_SLUG not detected or Octopus project name not passed in to \$1"; exit 1; fi
    
    echo "Proceeding with deployment using Octopus"
    echo $REPO_PATH
    if [[ -z $CI ]]; then
        #in order to prevent collisions in release names we use a timestamp so that we get pseudo 
        # sequential build numbers. This decreases to the risk of duplicate numbers down to a window 
        # of 1 second.
        #TODO add functionality to semversioner to get next version
        NEXTVERSION=`semversioner current-version | awk -F '[.]' '{ver=$1"."($2 + 1)"."$3; print ver}'`
        NEXTBUILD=$(date +%s)
        VERSION="$NEXTVERSION.${NEXTBUILD}-dev"
        echo "version = $VERSION"
    else
        #get version number and use it for version number in pipeline
        CURRENTVERSION=`semversioner current-version`
        VERSION="${CURRENTVERSION}.${BITBUCKET_BUILD_NUMBER}"
        echo "version = $VERSION"
    fi
    #TODO Add support for deploying to channels for development environment
    docker run --rm -t $(tty &>/dev/null && echo "-i") \
        -e AWS_ACCOUNT=${AWS_ACCOUNT} \
        -e OctopusServer=$OctopusServer \
        -e OctopusAPIKey=$OctopusAPIKey \
        -e BITBUCKET_BRANCH=$BITBUCKET_BRANCH \
        -v $REPO_PATH:/opt/atlassian/pipelines/agent/build \
        -w /opt/atlassian/pipelines/agent/build \
        --entrypoint /bin/bash \
        lob-docker.jfrog.io/devtools/octopus/${STAGE:-prd}:latest \
        -c "createANDdeployrelease.sh $PROJECTNAME $VERSION"
else #else deploy with gradle
    echo "build.gradle detected, proceeding with deployment using gradle"
    echo ${PROJECTDIR[0]}
    if [[ ${PROJECTDIR[0]} != "./" ]] 
    then
        chmod -R 777 ${PROJECTDIR[0]}/* #this is because file permissions aren't correct on pipeline artifacts
    fi
    cd ${PROJECTDIR[0]}
    gradle deploy
fi
