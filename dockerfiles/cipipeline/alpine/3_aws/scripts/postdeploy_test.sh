#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/node/postdeploy_test.sh, please change your code"
REGISTRY="lob-docker.jfrog.io"

if [[ "${ARTIFACTORY_USER}" != "" ]]
then
    docker login $REGISTRY -u $ARTIFACTORY_USER -p $ARTIFACTORY_PASSWORD
fi

OCTO_VER=`docker run --rm -t $(tty &>/dev/null && echo "-i") \
    -e OctopusServer=$OctopusServer \
    -e OctopusAPIKey=$OctopusAPIKey \
    --entrypoint /bin/bash \
    lob-docker.jfrog.io/devtools/octopus/prd:latest \
    -c "octo list-latestdeployments \
    --project=$BITBUCKET_REPO_SLUG \
    --environment=$BITBUCKET_BRANCH \
    --server=$OctopusServer \
    --apiKey=$OctopusAPIKey" \
    | grep Version: \
    | awk '{print $3}' \
    | sed -E -e "s/([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)\.[[:digit:]]+/\1/g" \
    | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"`
GIT_VER=`semversioner current-version`

echo "Octopus Version: ${OCTO_VER}"
echo "Git Version: ${GIT_VER}"

if [[ ${OCTO_VER} != ${GIT_VER} ]]
then
  echo "Versions do not match!"
  exit 1  
fi