#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/ for building, deploying and promoting, please change your code"
echo "Build the Artifact from Source"
#Find multiple package.json files
export PACKAGES=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
echo ${PACKAGES[@]}
set -e

COUNT=${#PACKAGES[@]}
for (( i=0; i<${COUNT}; i++ ))
do
    PACKAGE=${PACKAGES[$i]}
    echo "Package: ${PACKAGE}"
    pushd "${PACKAGE}"
        #This line is only needed for local execution
        rm -f *.zip
        if [[ -z ${CI} ]]
        then
            npm install
        else
            npm ci
        fi
        npm-pack-zip
        mv *.zip package.zip
        echo "package.zip file created"

        if [[ -f template.yml || -f swagger.yml ]]; then
            zip -r -X template.zip template.yml swagger.yml playbooks
            echo "template.zip file created"
        fi

        # look for the directory containing all of the IDP configuration for a given service
        if [[ -d idp ]]; then
            pushd idp
            zip -r -X ../idp.zip .
            popd
            echo "idp.zip file created"
        fi

        if [[ -d test ]]; then
            zip -r -X e2e.zip ./package.json ./package-lock.json ./test
            echo "e2e.zip file created"
        fi
    popd
done