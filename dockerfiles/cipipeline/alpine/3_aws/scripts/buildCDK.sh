#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/ for building, deploying and promoting, please change your code"
ROOT_DIR=$(pwd)
pushd $1
  PROJECT_DIR=$(pwd)
  echo "Build Cloud Formation Template in ${PROJECT_DIR}"
  rm -rf build/
  rm -rf *.zip
  mkdir -p build/
  assumeRole.sh npx cdk synth --output build > template.yml
  mv template.yml build/
  pushd build
    zip -r template.zip *
  popd
  mv build/*.zip ./template.zip
popd