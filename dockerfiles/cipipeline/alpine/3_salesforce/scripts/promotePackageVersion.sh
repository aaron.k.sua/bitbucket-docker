#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/promotePackageVersion.sh, please change your code"
set -e
source setEnvironment.sh
echo "...Promoting the latest package version of ${packageName} package"
source getPackageVersion.sh ${packageName}
currentPackageVersion=$(echo ${packageVersion} | awk -F '[.]' '{ver=$1"."$2"."$3; print ver}')

if [[ -d ".changes" ]]
then
  currentVersion=$(semversioner current-version)
  if [[ ${currentVersion} != {currentPackageVersion} ]]
  then
    echo "ERROR: Version in SalesForce does not match version in Repo.  Please investigate."
    exit "1"
  fi
fi
sfdx force:package:version:promote -p ${subscriberPackageVersionId} --noprompt
echo "Successfully promoted version ${packageVersion} of ${packageName} package"
echo "Creating git tag"
git tag ${packageVersion}
git push origin ${packageVersion}
echo "Tag created successfully"
