#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/createPackageVersion.sh, please change your code"
set -e
source setEnvironment.sh

if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
    git fetch origin refs/heads/integration
    INT_DIFF=$(git diff --raw HEAD..origin/integration | grep -v "\(changes\/\|CHANGELOG.md\)" || true)
    if [[ ${INT_DIFF} == "" ]]
    then
      echo "No diff, using existing Package"
      exit 0
    fi
fi

if [[ -z ${CI} ]]
then
  echo "You do not appear to be running in pipeline, exiting"
  exit 1
else
  if [[ -d ".changes/" ]]
  then
    echo "Setting version number with semversioner"
    CURRENTVERSION=`semversioner current-version`
    versionNumber="--versionnumber ${CURRENTVERSION}.${BITBUCKET_BUILD_NUMBER}"
  fi
fi

echo "...Creating a new package version of ${packageName} package"

sfdx force:package:version:create -v ${DEVHUBUSERNAME} ${versionNumber} --package ${packageName} --installationkey ${INSTALLKEY} --wait 110
source getPackageVersion.sh ${packageName}
echo "Successfully created version ${packageVersion} of ${packageName} package"
