#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/installPackageDependencies.sh, please change your code"
echo "...Installing Live Oak Labs package dependencies"
set -e
PACKAGEDETAILS=$(jq -c '.packageDirectories[]' $PWD/sfdx-project.json)
if [[ ${PACKAGEDETAILS} =~ dependencies ]]
then
  echo "Package has dependencies"
  IFS=$'\n'
  DEPENDENCIES=( $(echo ${PACKAGEDETAILS} | jq -c '.dependencies[]') )
fi
if [[ "${DEPENDENCIES[@]}" ]]
then
  for DEPENDENCY in "${DEPENDENCIES[@]}";
  do
    dependencyName=$(echo ${DEPENDENCY} | jq -r '.package')
    versionNumber=$(echo ${DEPENDENCY} | jq -r '.versionNumber')
    if [[ "$versionNumber" == "null" ]]
    then
      echo "WARNING The dependency ${dependencyName} has no version."
    else
      echo "...Searching for the latest package version of ${dependencyName} package"
      source getPackageVersion.sh ${dependencyName}
      echo "...Beginning install of version ${packageVersion} of ${dependencyName} package"
      install=$(sfdx force:package:install --package ${subscriberPackageVersionId} --targetusername ${USERNAME} --installationkey ${INSTALLKEY} --apexcompile package --wait 60 --publishwait 10)
      echo "Successfully deployed version ${packageVersion} of ${dependencyName} package"
    fi
  done
else
  echo "No dependencies detected"
fi
