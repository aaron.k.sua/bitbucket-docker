#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/runApexTestsAndReport.sh, please change your code"
echo "...Running Apex Tests"
testSuiteName=$(echo ${GIT_REPO} | tr '-' '_')
sfdx force:apex:test:run --testlevel RunSpecifiedTests --suitenames ${testSuiteName} --codecoverage --resultformat json --targetusername ${USERNAME} --wait 30000 > testresults.json
echo "...Outputting Apex Test results"
testRunID=$(cat testresults.json | jq -r '.result.summary.testRunId')
sfdx force:apex:test:report --targetusername ${USERNAME} --testrunid ${testRunID} --codecoverage --resultformat human

testStatus=$(cat testresults.json | jq -r '.result.summary.outcome')
echo "...Asserting Apex tests ${testStatus}"
if [ "${testStatus}" != "Passed" ]; then
    #output stacktrace and message for failures
    echo "$(cat testresults.json | jq -r '.result.tests[] | select(.StackTrace != null) | "\(.StackTrace) --> \(.Message)"')";
    exit 1;
fi

#Test Run code coverage gives accurate results only in a Scratch Org context. Native Salesforce inclusion of default salesforce classes impacts the results and can give a false negative or false positive.
if [[ ${ENVIRONMENT_NAME} == "CI Scratch Org" ]]
then
    codeCoverage=$(cat testresults.json | jq -r '.result.summary.testRunCoverage' | sed 's/.$//')
    echo "...Asserting Apex test code coverage of ${codeCoverage}% against required minimum of ${MINTESTCODECOVERAGE}%"
    if [ "${codeCoverage}" -lt "${MINTESTCODECOVERAGE}" ]; then
        echo "Increase code coverage to ${MINTESTCODECOVERAGE}%, current coverage: ${codeCoverage}%";
        exit 1;
    fi
fi