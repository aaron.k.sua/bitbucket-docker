#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/runLWCTestsAndReport.sh, please change your code"
#Find package.json files
PACKAGES=( $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
echo ${PACKAGES[@]}
set -e

COUNT=${#PACKAGES[@]}
for (( i=0; i<${COUNT}; i++ ))
do
    PACKAGE=${PACKAGES[$i]}
    pushd ${PACKAGE}
        echo "... running eslint"
        npm run lint

        echo "... running Jest unit tests for Lightning Web Components"

        # find LWC components. they must have a .xml metadata file to be a component.
        components=$(find ./force-app/main/default/lwc -name *.xml)

        if [ -z "$components" ]; then
            echo "Lightning Web Components were not found in the package"
        else
            npm run test:unit:cover
        fi
    popd
done