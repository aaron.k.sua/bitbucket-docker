#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/cleanupOrgs.sh, please change your code"
set -e
source setEnvironment.sh
echo "...Deleting CI Scratch Org"
sfdx force:org:delete -u CIScratchOrg -p --json
echo "Successfully Deleted CI Scratch Org"

exit ${BITBUCKET_EXIT_CODE};