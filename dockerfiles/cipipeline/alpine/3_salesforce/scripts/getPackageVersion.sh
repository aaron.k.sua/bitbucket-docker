#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/getPackageVersion.sh, please change your code"
set -e
# Takes an argument of the packageName referenced here as $1
content=$(sfdx force:package:version:list --json --concise -p $1 --orderby MajorVersion,MinorVersion,PatchVersion,BuildNumber | sed 's/[[:space:]]//g')
packageDetails=( $(echo $content | jq -c '.result[]') )
packageDetailsCount=${#packageDetails[@]}
for ((i=0; i<${packageDetailsCount}; i++))
do
  packageInfo=${packageDetails[$i]}
  packageVersion=$(echo $packageInfo | jq -r '.Version')
  subscriberPackageVersionId=$(echo $packageInfo | jq -r '.SubscriberPackageVersionId')
done