#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/pushToCIScratch.sh, please change your code"
set -e
echo "...Creating CI Scratch Org"
sfdx force:org:create -f config/project-scratch-def.json -a CIScratchOrg --setdefaultusername --wait 60
source installPackageDependencies.sh
echo "...Pushing SFDX Project to CI Scratch Org"
sfdx force:source:push -u CIScratchOrg -f
echo "Successfully Completed push to CI Scratch Org"
