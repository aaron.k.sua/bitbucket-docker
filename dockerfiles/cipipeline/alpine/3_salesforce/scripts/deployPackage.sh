#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/deployPackage.sh, please change your code"
set -e
echo "...Searching for the latest package version of ${packageName} package"
source getPackageVersion.sh ${packageName}
echo "...Beginning install of version ${packageVersion} of ${packageName} package to ${ENVIRONMENT_NAME}"
sfdx force:package:install --package "${subscriberPackageVersionId}" --installationkey ${INSTALLKEY} --targetusername ${USERNAME} --noprompt --apexcompile package --wait 60 --publishwait 30
echo "Successfully deployed version ${packageVersion} of ${packageName} package to ${ENVIRONMENT_NAME}"
