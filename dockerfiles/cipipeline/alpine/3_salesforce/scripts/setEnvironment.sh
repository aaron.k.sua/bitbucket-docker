#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of dockerfiles/devtools/bash/scripts/sfdx/setEnvironment.sh, please change your code"
set -e
sfdx --version
export GIT_REPO=$(git remote get-url origin | sed -E -e "s/.*\/(.*)(\.git|$)/\1/g")
export GIT_BRANCH=$(git branch | grep \* | awk '{print $2}')
export ENVIRONMENT_NAME=${BITBUCKET_BRANCH:-$GIT_BRANCH}
export DEVHUBKEY=${DEVHUBKEY:-3MVG9_XwsqeYoueIjgP01Ijjv1lgSZXwfJBB6S.6wy8TGmZIIZWPNzPfJCW4DNtu6tiHIsfhsbu76_DQaYVn4}
export DEVHUBUSERNAME=${DEVHUBUSERNAME:-lol-intdev@liveoak.bank}
export INSTALLKEY="liveoak123"
export packageName=$( cat sfdx-project.json | jq -r '.packageDirectories[0].package')
#Set minimum Apex test code coverage %
if [[ -z ${MINTESTCODECOVERAGE} ]]
then
  export MINTESTCODECOVERAGE='90'
fi


echo "Environment: ${ENVIRONMENT_NAME,,}"
case ${ENVIRONMENT_NAME,,} in
    feature/* )
        export ENVIRONMENT_NAME='CI Scratch Org'
        export CONSUMERKEY='CIScratchOrg'
        export USERNAME='CIScratchOrg'
        export LOGINURL="https://test.salesforce.com/"
        ;;
    integration )
        export ENVIRONMENT_NAME='Integration Fullcopy Sandbox'    
        export CONSUMERKEY='3MVG9Fy_1ZngbXqO6ZyUdcmUe1dRtT2l7CbsiyPkeUDBtspG9t3mbTApaK4rM9jW559xQ2wxKS5LeD8kQHhbs'
        export USERNAME='lol-intdev@liveoak.bank.integratio'
        export LOGINURL="https://test.salesforce.com/"
        ;;
    staging ) 
        export ENVIRONMENT_NAME='Staging Fullcopy Sandbox'    
        export CONSUMERKEY='3MVG9_I_oWkIqLrl7h.HM0nqegQ2oxmE3mxnsBb63XFVDpRjS37DsWvD3ZhZQ8S0Pd0_y3CsJ2.U.r4LWG5aV'
        export USERNAME='lol-intdev@liveoak.bank.staging'
        export LOGINURL="https://test.salesforce.com/"
        ;;        
    production ) 
        export ENVIRONMENT_NAME='Production Salesforce Org'    
        export CONSUMERKEY='3MVG9_XwsqeYoueIjgP01Ijjv1lgSZXwfJBB6S.6wy8TGmZIIZWPNzPfJCW4DNtu6tiHIsfhsbu76_DQaYVn4'
        export USERNAME='lol-intdev@liveoak.bank'
        export LOGINURL="https://login.salesforce.com/"
        ;;
    * )
        echo "Unrecognized Environment"
        exit 1
        ;;
esac

#Decrypts a connected app key and outputs a file
openssl enc -d -aes-256-cbc -in build/server.key.enc -out build/server.key -K ${AESKEY} -iv ${IV}
echo "...Authenticating to DevHub"
sfdx force:auth:jwt:grant --clientid ${DEVHUBKEY} --jwtkeyfile build/server.key --username ${DEVHUBUSERNAME} --setdefaultdevhubusername

if [[ ${ENVIRONMENT_NAME} != "CI Scratch Org" ]]
then
    echo "...Authenticating to ${ENVIRONMENT_NAME}"
    sfdx force:auth:jwt:grant --clientid ${CONSUMERKEY} --jwtkeyfile build/server.key --username ${USERNAME} -r ${LOGINURL} --setdefaultusername
fi
