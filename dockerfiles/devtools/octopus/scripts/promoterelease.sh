#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/promoterelease.sh, please change your code"
#Command line parameters
# $1 = Octopus project name.  Ideally, this will be the name of the repo.
# $2 = Version number to use for package and Octopus release.  

#If deploying from pipelines, use branch name for environment, otherwise deploy to developer environment
deployment_env=${BITBUCKET_BRANCH:-Developer}

case ${deployment_env}
in
    staging)
        source_env="integration"
        ;;
    production)
        source_env="staging"
        ;;
    *)
        echo "not running from the correct branch, nothing to promote"
        exit 1
        ;;
esac

octo promote-release --project=$1 --from=${source_env} --deployto=${deployment_env} --progress --server=$OctopusServer --apiKey=$OctopusAPIKey

echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/promoterelease.sh, please change your code"