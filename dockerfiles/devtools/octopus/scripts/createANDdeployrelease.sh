#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/createANDdeployrelease.sh, please change your code"
#Command line parameters
# $1 = Octopus project name.  Ideally, this will be the name of the repo.
# $2 = Version number to use for package and Octopus release.  

set -e

#If deploying from pipelines, use branch name for environment, otherwise deploy to developer environment
deployment_env=${BITBUCKET_BRANCH:-Developer}

version=$2

case ${deployment_env}
in
    Developer)
        deployment_channel="Developer"
        ;;
    integration)
        deployment_channel="Default"
        ;;
    staging)
        deployment_channel="Staging"
        ;;
    *)
        echo "Deploying directly to ${deployment_env} is not allowed."
        exit 1
        ;;
esac

echo "Selecting channel ${deployment_channel} for release creation..."
octoPushes=0
if [[ -f package.zip ]]; then
    mv package.zip $1.${version}.zip
    octo push --package=$1.${version}.zip --overwrite-mode=OverwriteExisting --server=${OctopusServer} --apiKey=${OctopusAPIKey}
    octoPushes=$((octoPushes+1))
else
    echo "No package.zip file detected."
fi

ZIP_FILES=(
template 
idp
e2e
)
ZIP_FILE_LEN=${#ZIP_FILES[@]}

for (( i=0; i<${ZIP_FILE_LEN}; i++ ));
do
    zipfile=${ZIP_FILES[$i]}
    if [[ -f ${zipfile}.zip ]]; then
        mv ${zipfile}.zip $1-${zipfile}.${version}.zip
        octo push --package=$1-${zipfile}.${version}.zip --overwrite-mode=OverwriteExisting --server=${OctopusServer} --apiKey=${OctopusAPIKey}
        octoPushes=$((octoPushes+1))
    else
        echo "No $zipfile.zip file detected."
    fi
done

if [[ ${octoPushes} == 0 ]]
then
  echo "ERROR: No zip files detected to deploy"
  exit 1
fi

octo create-release --project=$1 --version=${version} --package=$1:${version} --channel ${deployment_channel} --server=${OctopusServer} --apiKey=${OctopusAPIKey} --ignoreexisting

octo deploy-release \
--deployto=${deployment_env} \
--variable=AWS_ACCOUNT:${AWS_ACCOUNT} \
--project=$1 \
--version=${version} \
--progress \
--timeout=3600 \
--waitfordeployment \
--deploymenttimeout=01:00:00 \
--server=${OctopusServer} \
--apiKey=${OctopusAPIKey}

# do cleanup
rm -f $1.${version}.zip
for (( j=0; j<${ZIP_FILE_LEN}; j++ ));
do
    zipfile=${ZIP_FILES[$j]}
    rm -f $1-${zipfile}.${version}.zip
done

echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/createANDdeployrelease.sh, please change your code"