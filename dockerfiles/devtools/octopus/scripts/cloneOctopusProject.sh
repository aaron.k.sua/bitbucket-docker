#!/bin/bash
echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/cloneOctopusProject.sh, please change your code"
if [ $# != 3 ]; then
    echo "script requires 3 parameters as follows:"
    echo "\$1 is the name of the new project to create"
    echo "\$2 is the ID of the project you wish to clone"
    echo "\$3 is the ID of the project group to place your new project in"
    exit
fi

#check for existing project
projectId="Projects-$2"
projectGroupId="ProjectGroups-$3"
checkForProject=`curl -s -X GET "$OctopusServer/api/projects/$1" -H "accept: application/json" -H "X-Octopus-ApiKey: $OctopusAPIKey"`

if [ $(echo $checkForProject | jq -r '.Name') = $1 ]; then 
    echo "Octopus Project $1 already exists"
    exit 1
else
    echo "Project $1 does not exist in Octopus.  Creating project now"
    jsonvar=$(jq -n --arg ProjectName "$1" --arg ProjectGroup "$projectGroupId" '{
        "Name":$ProjectName,
        "LifecycleId":"Lifecycles-1",
        "ProjectGroupId":$ProjectGroup
    }')

createProject=`curl -s -X POST "$OctopusServer/api/projects?clone=$projectId&ApiKey=$OctopusAPIKey" -H "accept: application/json" -H "content-type: application/json" -d "$jsonvar"`
echo $(echo $createProject | jq -r '.Name') "created successfully"
fi

echo "WARNING WARNING WARNING This script is being deprecated in favor of bash/scripts/octopus/cloneOctopusProject.sh, please change your code"