## Overview
### octopus docker images have a few scripts for deploying and promoting from octopus:
```
createANDdeployrelease.sh       promoterelease.sh       cloneOctopusProject.sh      createOctopusAWSAccount.sh
```

### Prereqs
 - [Configure local to work with Octopus](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/772505786/Configure+local+to+work+with+Octopus)

### Script Details

#### cloneOctopusProject
Example execution:
```
octocreate myNewOctopusProject 101 45
```

$1 is the name of the new project  to create

$2 is the ID of the project you wish to clone

$3 is the ID of the project group to place your new project in

for details of Project Name and Project Group Name please see [here](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/772505818/Clone+a+template+Octopus+Project)

#### createANDdeployrelease.sh
no additional arguments.  executed from the root of your repo to build and deploy your project.
- see [How to deploy using Octopus](https://jensyn.atlassian.net/wiki/spaces/DVT/pages/772898939/How+to+deploy+using+Octopus)

#### promoterelease.sh

#### createOctopusAWSAccount.sh