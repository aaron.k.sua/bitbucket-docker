## Houses scripts to help with managing LOB projects in AWS and Salesforce
### Pipeline Common
```
buildOrPromote.sh       promote.sh              set_ssh.sh
buildAndDeploy.sh       deployOrPromote.sh      deploy.sh             
rev_version.sh          set_dockerrc.sh
```
### NOTES

#### ...OrPromote.sh scripts
These are deprecated and only present to ease the transition from the legacy cipipeline images and pipelines.  They leverage git to determine if there are changes between branches during a merge that would mean a promote is possible.

#### deploy.sh and promote.sh:
Octopus project name needs to be passed in as a positional argument
$1 = Octopus project name.  Ideally, this will be the name of the repo.

When deploying to Octopus, this script expects the following environment variables to be set/available:
- OctopusServer
- OctopusAPIKey
- AWS_ACCOUNT

### Salesforce
```
cleanupOrgs.sh                  deployPackage.sh                promotePackageVersion.sh        runTestsAndReport.sh
createPackageVersion.sh         getPackageVersion.sh            pushToCIScratch.sh              setEnvironment.sh
deploy-ci.sh                    installPackageDependencies.sh   pushToOrg.sh                    set_ssh.sh
buildOrPromote.sh                installdeps.sh                 postdeploy_test.sh              promote.sh
runLWCTestsAndReport.sh
```

### tools
```
ansible             assumeRole              aws
cdk                 dynamodb                node
npm                 npx                     octoclone
octopus             python3                 semversioner
```

#### assumeRole and aws commands
assumeRole and aws support several methods for managing credentials used to connect to AWS.
The native AWS Profile Configuration, Profiles as .env files, and the established Environment Variables

- The native AWS CLI and CDK profiles typically stored in ${HOME}/.aws
There are two files ${HOME}/.aws/config and ${HOME}/.aws/credentials
[AWS Documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)

example:

config:

```
[developer]
aws_account = <your aws account>
output = json
region = us-east-1

[integration]
aws_account = <integration aws account>
role_arn = AWSReadAccessRole
output = json
region = us-east-1

[staging]
aws_account = <staging aws account>
role_arn = AWSReadAccessRole
output = json
region = us-east-1

[production]
aws_account = <production aws account>
role_arn = CloudWatchLogReadAccessRole
output = json
region = us-east-1
```

credentials:

```
[developer_creds]
aws_access_key_id = <your aws access key>
aws_secret_access_key = <your aws secret>

[integration]
role_arn = arn:aws:iam::<integration account>:role/AWSReadAccessRole
source_profile = developer_creds

[staging]
role_arn = arn:aws:iam::<staging account>:role/AWSReadAccessRole
source_profile = developer_creds

[production]
role_arn = arn:aws:iam::<production account>:role/CloudWatchLogReadAccessRole
source_profile = developer_creds
```

- The .env files housed in ${HOME}/.aws_profiles/.  Each Profile/Environment is a separate file

example:

developer.env

```
AWS_ACCOUNT=<your aws account>
AWS_ACCESS_KEY_ID=<your aws access key>
AWS_SECRET_ACCESS_KEY=<your aws secret>
AWS_ROLE=Developer
AWS_DEFAULT_REGION=${$AWS_REGION:-us-east-1}
```

integration.env

```
AWS_ACCOUNT=<integration account>
AWS_ACCESS_KEY_ID=<your aws access key>
AWS_SECRET_ACCESS_KEY=<your aws secret>
AWS_ROLE=AWSReadAccessRole
AWS_DEFAULT_REGION=us-east-1
```

- As well as whatever ENV VARS are set prior to executing the commands.


```
-p for PROFILE, The profile or environment to use, do not include .env if using aws_profiles/ method
                   for the aws script if you specify CONFIG you must also specify the PROFILE to use.
-c for CONFIG, the path to the standard AWS profiles config file
-s for STAGE, the stage of container to utilze, typically 'dev', 'int', or 'prd'
```

### node
```
buildCDK.sh         buildNPMPack.sh         installNodeDependencies.sh
postbuild_test.sh   prebuild_test.sh        publish.sh
set_npmrc.sh
```
NOTE:

- publish.sh is to publish node modules to artifactory
- set_npmrc.sh handles authentication with Artifactory for npm management