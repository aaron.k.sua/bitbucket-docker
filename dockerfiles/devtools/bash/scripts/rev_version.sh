#!/bin/bash
#This script is used to add versioning to a repo and is intended to be run on one of the Eternal Branches of Production, Staging, or Integration.  It leverages the semversioner python package.
set -e
#use git commands to determine if commit is a merge coming from the Integration branch.
PARENT_HASH=($(git --no-pager  log --pretty=%P -n 1))
if [[ ${#PARENT_HASH[@]} == 1 ]]
then
  echo "ERROR ERROR ERROR This commit is either a squash or a direct commit to the branch"
  exit 1
fi
echo "fetching all other branches"
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
IS_SHALLOW=$(git rev-parse --is-shallow-repository)
if [[ ${IS_SHALLOW} == "true" ]]
then 
    git fetch --unshallow
else
    git remote set-branches origin '*'
    git fetch --all
fi

echo "fetch complete"
git checkout production
echo "production branch checkout successful"

git checkout integration
echo "integration branch checkout successful"
INTEGRATION_HASH=($(git rev-parse integration))
echo "integration branch head hash successful"

git checkout staging
echo "staging branch checkout successful"
STAGING_HASH=($(git rev-parse staging))
echo "staging branch head hash successful"

git checkout ${BITBUCKET_BRANCH:-staging}
echo "${BITBUCKET_BRANCH:-staging} branch checkout successful"
git fetch
echo "second git fetch successful"

DIFF=$(git diff ${BITBUCKET_BRANCH:-staging} origin/${BITBUCKET_BRANCH:-staging})
echo "branch diff successful"
#Check if local copy is behind remote, if so skip versioning
if [[ ${DIFF} == "" ]]
then
    #check for existence of bitbucket-pipelines.yml.  Versioning needs to occurr at root of repo
    FILE=bitbucket-pipelines.yml
    if [[ -f ${FILE} ]]
    then
        echo "Checking for existence of $FILE has succeeded.  Proceeding with versioning."
        if [[ $(ls .changes/*.*.0.json | wc -l) -gt 1 ]]
        then
          echo "version file history detected, continuing"
        else
          echo "ERROR: You need at least two starting version files in .changes/"
          exit 1
        fi
        #Check for changes between the current working branch and production.
        echo "Determining what the current working branch is..."
        CURRENTBRANCH=$(git branch | grep \* | awk '{print $2}')
        echo "Current working branch is set to ${CURRENTBRANCH}"
        echo "Calculating changes with previous production commit"
        if [[ ${CURRENTBRANCH} == 'production' ]]
        then
            CHANGES=$(git --no-pager log --format=%B ${PARENT_HASH[0]}...production)
        else
            CHANGES=$(git --no-pager log --format=%B ${CURRENTBRANCH}...production)
        fi
        echo "Done"

        if [[ ${CHANGES} ]]
        then
            CURRENTVERSION=$(semversioner current-version)
            rm -rf .changes/${CURRENTVERSION}.json
            echo "Updating change log for version ${CURRENTVERSION}"
            echo "${CURRENTVERSION}"
            #Determine if changes are minor or patch based on branch name prefix
            #Only add commit message lines that contain a Jira ticket ID
            MINOR=$(echo ${CHANGES} | grep -o -E "[A-Za-z\/]*[A-Z][A-Z]+-[0-9]+" | grep -v hotfix | sort -V -u)
            PATCH=$(echo ${CHANGES} | grep -o -E "hotfix\/[A-Z][A-Z]+-[0-9]+" | sort -V -u)

            #We will not rev the major version number.  If we need to rev major version, create a new repo
            if [[ ${MINOR} ]]
            then
                echo "Minor release detected"
                semversioner add-change --type minor --description \"${MINOR}\"
            elif [[ ${PATCH} ]]
            then
                echo "Patch release detected"
                semversioner add-change --type patch --description \"${PATCH}\"
            else
                echo "WARNING: No tickets found in commit history, assuming MINOR release"
                semversioner add-change --type minor --description \"Assumed minor release, no tickets found\"
            fi
          
            #trigger a new release version
            RELEASE=$(semversioner release)
            #capture new version number into a variable for later use
            VERSION=$(echo ${RELEASE} | grep -o -E "new release: [0-9]+.[0-9]+.[0-9]+" | awk '{print $3}')

            if [[ ${CURRENTVERSION} != ${VERSION} ]]
            then
                echo "CURRENT VERISION and VERSION didn't match, panicking"
                exit 1
            fi

            #add to the change log
            echo "Copy in the changelog"
            semversioner changelog > CHANGELOG.md

            #echo "Version has changed.  Old version number = $CURRENTVERSION.  New  version number = $VERSION"
            if [[ $(git diff --name-only) != "" ]]
            then
                echo "Committing new change log"
                git commit -a -m "Update Version Change Log [skip CI]"
                git push
            fi
        else
        git reset --hard HEAD
        fi
      
        if [[ ${PARENT_HASH[1]} == ${INTEGRATION_HASH[0]} || ${PARENT_HASH[1]} == ${STAGING_HASH[0]} ]] #if this was a merge from integration or staging
        then
            echo "Updating integration version"
            SAMPLE='[
                {
                "description": "TBD",
                "type": "minor"
                }
            ]'
            git checkout integration
            NEXT_VERSION=$(semversioner current-version | awk -F '[.]' '{ver=$1"."($2 + 1)"."$3; print ver}')
            echo ${SAMPLE} >> .changes/${NEXT_VERSION}.json
            git pull
            git add .changes/${NEXT_VERSION}.json
            git commit -m "add next version: ${NEXT_VERSION} [skip CI]"
            git push
            git tag ${NEXT_VERSION}
            git push origin ${NEXT_VERSION}
            echo "Integration version updated, changing back to pipeline branch ${BITBUCKET_BRANCH}"
            git checkout ${BITBUCKET_BRANCH}
            git reset --hard HEAD
            echo "Changed branch to ${BITBUCKET_BRANCH}"
        fi
    #add logic for version number to use when deploying from local
    else
        echo "${FILE} not detected.  The versioning script is not being run in the root of your repository or your repo has not enabled pipelilnes."
        exit 1
    fi
else
    echo "WARNING:  The remote appears to have changes not in this commit.  If this is not a rerun of an old job manual intervention may be required."
fi
