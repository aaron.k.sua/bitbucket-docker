#!/bin/bash

#Command line parameters
# $1 = Octopus project name.  Ideally, this will be the name of the repo.
# $2 = Version number to use for package and Octopus release.

#If deploying from pipelines, use branch name for environment, otherwise deploy to developer environment
deployment_env=${BITBUCKET_BRANCH:-Developer}
echo "look for staging branch"
git remote set-branches origin '*'
git fetch origin staging
branches=( $(git branch --list -a | grep staging) )
echo "search for staging branch complete"

case ${deployment_env}
in
    staging)
        echo "Destination is Staging and Source is Integration"
        source_env="integration"
        ;;
    production)
        if [[ -z ${branches} ]]
        then 
            echo "Destination is Production and Source is Integration"
            source_env="integration"
        else
            echo "Destination is Production and Source is Staging"
            source_env="staging"
        fi
        ;;
    *)
        echo "not running from the correct branch, nothing to promote"
        exit 1
        ;;
esac

echo "Running Promote in Octopus"
octo promote-release --project=$1 --from=${source_env} --deployto=${deployment_env} --progress --server=$OctopusServer --apiKey=$OctopusAPIKey
