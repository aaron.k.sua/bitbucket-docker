#!/bin/bash

jsonvar=$(jq -n --arg AccessKey "$AWS_ACCESS_KEY_ID" --arg SecretKey "$AWS_SECRET_ACCESS_KEY" --arg AccountName "$1" '{
  "AccountType":"AmazonWebServicesAccount",
  "AccessKey":$AccessKey,
  "SecretKey":
    { 
      "HasValue":true,
      "NewValue":$SecretKey
    },
  "Name":$AccountName,
  "Description":"",
  "TenantedDeploymentParticipation":"Untenanted",
  "TenantTags":[],
  "TenantIds":[],
  "EnvironmentIds":["Environments-1"]
}')

createAccount=`curl -X POST "$OctopusServer/api/accounts?ApiKey=$OctopusAPIKey" -H "accept: application/json" -H "content-type: application/json" -d "$jsonvar"`

echo $createAccount

if [ $(echo $createAccount | jq -r '.AccountType') == "AmazonWebServicesAccount" ]
then
    echo "Octopus AWS Account created successfully"
    #Adding account name to environment variable "OctopusAWSAccount" after successful creation
    echo "export OctopusAWSAccount=$1" >> ~/.profile
else
  echo "Account not created"
fi
