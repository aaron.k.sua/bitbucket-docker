#!/bin/bash
REGISTRY="lob-docker.jfrog.io"

if [[ "${ARTIFACTORY_USER}" != "" ]]
then
    echo "${ARTIFACTORY_PASSWORD}" | docker login ${REGISTRY} -u ${ARTIFACTORY_USER} --password-stdin
fi