#!/bin/bash
echo "WARNING WARNING WARNING  **************DEPRECATED*******************"
echo "For use in migrating legacy implementation only"
echo "WARNING WARNING WARNING  **************DEPRECATED*******************"
echo "Build a New Artifact or Promote existing Artifact"
set -e
#use git commands to determine if commit is a merge coming from the Integration branch
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin refs/heads/integration
INT_DIFF=$(git diff --raw HEAD..origin/integration | grep -v "\(changes\/\|CHANGELOG.md\)" || true)

if [[ ${INT_DIFF} == "" ]]
then
  echo "No diff, promote Integration to Staging"
  promote.sh
else
  echo "Diff detected, deploying new build Artifact"
  deploy.sh
fi