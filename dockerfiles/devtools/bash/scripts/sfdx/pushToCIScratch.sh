#!/bin/bash
set -e
echo "...Creating CI Scratch Org"
sfdx force:org:create -f config/project-scratch-def.json -a CIScratchOrg --setdefaultusername --wait 60
source installPackageDependencies.sh
echo "...Pushing SFDX Project to CI Scratch Org"
sfdx force:source:push -u CIScratchOrg -f
echo "Successfully Completed push to CI Scratch Org"
