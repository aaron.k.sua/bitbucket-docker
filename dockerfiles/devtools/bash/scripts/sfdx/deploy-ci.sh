#!/bin/sh

# Variables section
TMPDIR=/tmp/ui-portal-dev/$1
REPOSITORY_URL="git@bitbucket.org:liveoakbank/$1.git"
BRANCH_NAME=$2
DEPENDENCIES_DIR="dependencies/installedPackages"
REPO=$1

# Check for existing tmp dir and if so remove it, then create it
if [ -d "$TMPDIR" ]; then
	printf '%s\n' "Removing existing directory: $TMPDIR"
	rm -rf "$TMPDIR"
fi
mkdir -p "$TMPDIR"

# Try to clone git repo into the tmp dir
echo "Cloning repo into directory: $TMPDIR"
git clone -b "$BRANCH_NAME" "$REPOSITORY_URL" "$TMPDIR"
if [ "$?" = "0" ]; then
	echo "Repo downloaded successfuly!"
else
	echo "Error while fetching remote branch! Aborting" 1>&2
	exit 1
fi

# Remove uneeded dependencies (TODO: Put these in an array or something)
rm -f "$TMPDIR/$DEPENDENCIES_DIR/API_CHR.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/LOB_TDF.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/LiveOak.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/LLC_BI.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/nFORCE.installedPackage"
rm -f "$TMPDIR/$DEPENDENCIES_DIR/nDESIGN.installedPackage"

# Change to tmp dir and execute the ant deploy command
echo "cd to $TMPDIR..."
cd "$TMPDIR"
echo "checkout $BRANCH_NAME"
git checkout "$BRANCH_NAME"
if [ "$BRANCH_NAME" =  "master" ]; then
	echo "add starter as upstream ..."
	git remote add upstream git@bitbucket.org:liveoakbank/starter.git
	echo "fetch upstream ..."
	git fetch upstream
	echo "merge upstream master..."
	git merge --no-edit upstream/master
else
	echo "Deploying to CI ..."
	ant -debug deployCI
fi
exitcode=$?

if [ $exitcode -eq  0 ]; then
	echo "CI deployment and test runs are successful"
	if [ "$BRANCH_NAME" = "master" ]; then
		echo "Pushing to $REPOSITORY_URL branch $BRANCH_NAME"
		git push origin "$BRANCH_NAME"
	fi
else
	echo "ANT deployCI returned with $exitcode"
fi

exit $exitcode
