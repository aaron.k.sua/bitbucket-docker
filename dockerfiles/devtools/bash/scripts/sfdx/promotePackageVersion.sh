#!/bin/bash
set -e
source setEnvironment.sh
echo "...Promoting the latest package version of ${packageName} package"
source getPackageVersion.sh ${packageName}
currentPackageVersion=$(echo ${packageVersion} | awk -F '[.]' '{ver=$1"."$2"."$3; print ver}')
echo "Salesforce version: ${currentPackageVersion}"

if [[ -d ".changes" ]]
then
  currentVersion=$(semversioner current-version)
  echo "Repo version: ${currentVersion}"
  if [[ ${currentVersion} != ${currentPackageVersion} ]]
  then
    echo "ERROR: Version in SalesForce does not match version in Repo.  Please investigate."
    exit "1"
  fi
fi
sfdx force:package:version:promote -p ${subscriberPackageVersionId} --noprompt
echo "Successfully promoted version ${packageVersion} of ${packageName} package"

echo "Creating git tag"
checkTag=$(git tag -l ${packageVersion})
if [[ -z ${checkTag} ]]
then 
  git tag ${packageVersion}
  git push origin ${packageVersion}
fi
echo "Tag created successfully"
echo "Advancing minor and patch version using semversioner"
