#!/bin/bash
echo "...Running Apex Tests"
if [[ -z ${RETRY_LIMIT} ]]
then
  RETRY_LIMIT=3
fi

echo "Finding class files"
TEST_CLASS_FILES=( $(find . -name "*Test.cls" -exec basename \{} .cls \;) )
if [ ${#TEST_CLASS_FILES[@]} == 0 ]
then
  echo "No test class files were detected, please add at least one .*Test.cls named file to run this script"
  exit 1
elif [ ${#TEST_CLASS_FILES[@]} == 1 ]
then
  echo "one test class found, running synchronously"
  TESTS="${TEST_CLASS_FILES[0]} --synchronous "
else
  echo "multiple test class files found"
  TESTS="${TEST_CLASS_FILES[@]}"
  TESTS="${TESTS//$' '/','}"
fi

for (( RETRY_COUNT=1; RETRY_COUNT <= ${RETRY_LIMIT}; RETRY_COUNT++ ))
do
  sfdx force:apex:test:run --testlevel RunSpecifiedTests --classnames ${TESTS} --codecoverage --resultformat json --targetusername ${USERNAME} --wait 30000 > testresults.json
  echo "...Outputting Apex Test results"

  testStatus=$(cat testresults.json | jq -r '.result.summary.outcome')
  echo "...Asserting Apex tests ${testStatus}"
  if [ "${testStatus}" != "Passed" ]; then
      #output stacktrace and message for failures
      echo "$(cat testresults.json | jq -r '.result.tests[] | select(.StackTrace != null) | "\(.FullName)\n  \(.StackTrace) --> \(.Message)"')";
      exit 1;
  fi
  set -e

  echo "...Asserting Apex test code coverage against required minimum of ${MINTESTCODECOVERAGE}%"

  CLASS_FILES=$(find . -name "*.cls" -not -name *Test.cls -exec basename \{} .cls \;)

  ALL_RESULTS=$(\
  jq --arg inarr "${CLASS_FILES}" '($inarr |
  split("\n")) as $whitelist |
  [
    .result.coverage.records[] |
    select( .ApexClassOrTrigger.Name as $name | any( $whitelist[]; . == $name) ) |
    {
      "ApexClass": .ApexClassOrTrigger.Name,
      "coveredLines": .Coverage.coveredLines,
      "uncoveredLines": .Coverage.uncoveredLines
    }
  ]
  | group_by(.ApexClass)
  | map( (.[0]|del(.coveredLines, .uncoveredLines)) +
    {
      coveredLines: (map(.coveredLines[])|unique_by(.)),
      uncoveredLines: (map(.uncoveredLines[])|unique_by(.))
    })
  | .[] | . + { uncoveredLines: (.uncoveredLines - .coveredLines) }
  | {
      "Apex Class": .ApexClass,
      "Total Covered": (.coveredLines | length),
      "Total Lines": ((.uncoveredLines|length)+(.coveredLines | length)),
      "Uncovered Lines": (.uncoveredLines | join(", "))
    }
    ' \
    testresults.json
  )
  if [[ ${ALL_RESULTS} == "" ]]
  then
    echo "Yes, you have no bananas (test results) today!"
    exit 1
  fi

  TOTAL_RESULTS=$(echo $ALL_RESULTS | \
    jq -nr 'reduce (inputs | to_entries[]) as {$key,$value} ({}; .[$key] += $value) |
    del(."Apex Class") | del(."Uncovered Lines") |
    . + { "Code Coverage": (((."Total Covered"/."Total Lines")*100)|floor|tostring +"%%") }' \
  )

  MISSING_TESTS=( $(\
  jq -r '
  { records: [
    .result.coverage.records[] |
      .TestMethodName
    ],
    tests: [
      .result.tests[] |
      .MethodName
    ]
  } |
  .tests - .records | .[]
    ' \
    testresults.json
  ) )

  COVERAGE=$(echo ${TOTAL_RESULTS} | jq -r '."Code Coverage"' | tr -d '%')

  if [[ ${#MISSING_TESTS[@]} > 0 ]]
  then
    printf "\nAn Unknown Error Occurred, will rerun tests.\n
    Retry Number ${RETRY_COUNT} of ${RETRY_LIMIT}\n
    Missing Test Results:\n"
    printf '%s\n' "${MISSING_TESTS[@]}"

    if [[ ${RETRY_COUNT} == ${RETRY_LIMIT} ]]
    then
      exit 1
    else
      printf "\n---------RETRYING-----------\n"
      continue
    fi
  else
    break
  fi
done

printf "\n------------TEST COVERAGE--------------\n"
printf "${ALL_RESULTS//$',\n'/'\n  '}" | tr -d '{}"'

printf "\n------------TOTAL COVERAGE-------------\n"
printf "$TOTAL_RESULTS\n" | tr -d '{},"'

if [[ ${COVERAGE} -lt ${MINTESTCODECOVERAGE} ]]
then
  echo "Increase code coverage to ${MINTESTCODECOVERAGE}%, current coverage: ${COVERAGE}%";
  exit 99
else
  echo "Impressive, most impressive."
  exit 0
fi