#!/bin/bash
set -e
export GIT_REPO=$(git remote get-url origin | sed -E -e "s/.*\/([A-Z a-z 0-9 _-]*)(\.git|$)/\1/g")
export GIT_BRANCH=$(git branch | grep \* | awk '{print $2}')
export ENVIRONMENT_NAME=${BITBUCKET_BRANCH:-$GIT_BRANCH}
export DEVHUBKEY=$PRODUCTION_CONSUMER_KEY
export DEVHUBUSERNAME=${DEVHUBUSERNAME:-lol-intdev@liveoak.bank}
export INSTALLKEY="liveoak123"
export packageName=$( cat sfdx-project.json | jq -r '.packageDirectories[0].package')
#Set minimum Apex test code coverage %
if [[ -z ${MINTESTCODECOVERAGE} ]]
then
  # This can be an account variable in our git hub provider.
  export MINTESTCODECOVERAGE='90'
fi

printf '\n***\n'
echo "Set Deployment Variables to: ${ENVIRONMENT_NAME,,}"
printf '********************************************\n\n'

case ${ENVIRONMENT_NAME,,} in
    feature/*|bugfix/*|hotfix/* )
        echo '...'
        export ENVIRONMENT_NAME='CI Scratch Org'
        export CONSUMERKEY='CIScratchOrg'
        export USERNAME='CIScratchOrg'
        export LOGINURL="https://test.salesforce.com/"
        ;;
    integration )
        export ENVIRONMENT_NAME='Integratio -- Fullcopy Sandbox'  
        export CONSUMERKEY=$INTEGRATION_CONSUMER_KEY
        export USERNAME='lol-intdev@liveoak.bank.integratio'
        export LOGINURL="https://test.salesforce.com/"
        ;;
    staging ) 
        export ENVIRONMENT_NAME='Staging -- Fullcopy Sandbox'  
        export CONSUMERKEY=$STAGING_CONSUMER_KEY
        export USERNAME='lol-intdev@liveoak.bank.staging'
        export LOGINURL="https://test.salesforce.com/"
        ;;      
    production ) 
        export ENVIRONMENT_NAME='Production'  
        export CONSUMERKEY=$PRODUCTION_CONSUMER_KEY
        export USERNAME='lol-intdev@liveoak.bank'
        export LOGINURL="https://login.salesforce.com/"
        ;;
    * )
        echo "Unrecognized Environment"
        exit 1
        ;;
esac

#Decrypts a connected app key and outputs a file
printf '\n*****\n'
echo "Decrypting the encrypted key for JWT Auth"
printf '*********************************************\n\n'
openssl enc -d -aes-256-cbc -in build/server.key.enc -out build/server.key -K ${AESKEY} -iv ${IV}

printf '\n*******\n'
echo "Key Decryption Complete - Authenticating to DevHub - Org2 Production"
printf '*********************************************\n\n'

sfdx force:auth:jwt:grant --clientid ${DEVHUBKEY} --jwtkeyfile build/server.key --username ${DEVHUBUSERNAME} --setdefaultdevhubusername

if [[ ${ENVIRONMENT_NAME} != "CI Scratch Org" ]]
then

    printf '\n*********\n'
    echo "Performing JWT authenctation to deployment stage: ${ENVIRONMENT_NAME}"
    printf '*********************************************\n\n'

    sfdx force:auth:jwt:grant --clientid ${CONSUMERKEY} --jwtkeyfile build/server.key --username ${USERNAME} -r ${LOGINURL} --setdefaultusername

    printf '\n***********\n'
    echo "Authenticated to: ${ENVIRONMENT_NAME}"
    printf '*********************************************\n\n'

fi
