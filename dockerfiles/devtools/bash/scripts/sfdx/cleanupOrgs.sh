#!/bin/bash
set -e
source setEnvironment.sh
echo "...Deleting CI Scratch Org"
sfdx force:org:delete -u CIScratchOrg -p --json
echo "Successfully Deleted CI Scratch Org"

exit ${BITBUCKET_EXIT_CODE};