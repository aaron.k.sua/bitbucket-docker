#!/bin/bash
set -e

if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
    git fetch origin refs/heads/integration
    INT_DIFF=$(git diff --raw HEAD..origin/integration | grep -v "\(changes\/\|CHANGELOG.md\)" || true)
    if [[ ${INT_DIFF} == "" ]]
    then
      echo "No diff, using existing Package"
      exit 0
    fi
fi

if [[ -z ${CI} ]]
then
  echo "You do not appear to be running in pipeline, exiting"
  echo "export CI=true"
  exit 1
else
  if [[ -d ".changes/" ]]
  then
    printf '\n**************\n'
    echo "Reading version information from /.changes using semversioner"
    printf '*********************************************\n\n'

    CURRENTVERSION=$(semversioner current-version)

    printf '\n****************\n'
    echo "Building Version: ${CURRENTVERSION}.NEXT, version information available once package is created."
    printf '*********************************************\n\n'

    versionNumber="--versionnumber ${CURRENTVERSION}.NEXT"

  fi
fi

printf '\n***********************\n'
echo "Creating a new version of ${packageName}!!!!. This will take a few minutes..."
printf '*********************************************\n\n'

sfdx force:package:version:create --codecoverage -v ${DEVHUBUSERNAME} ${versionNumber} --package ${packageName} --installationkey ${INSTALLKEY} --wait 110

printf '\n****************************\n'
echo "Requesting latest package version for ${packageName} from DevHub"
printf '*********************************************\n\n'
source getPackageVersion.sh ${packageName}

printf '\n*******************************\n'
echo "Successfully created NEXT version: ${packageVersion} of ${packageName}"
printf '*********************************************\n\n'

git fetch --all
DIFF=$(git diff ${BITBUCKET_BRANCH:-HEAD} origin/${BITBUCKET_BRANCH:-HEAD})
echo "branch diff successful"
#Check if local copy is behind remote, if so skip versioning
if [[ ${DIFF} == "" ]]
then
    printf '\n************\n'
    echo "Writing versionNumber=${pkgVersion} in sfdx-project.json and committing to staging."
    printf '\n******************************************\n\n'

    cat <<< $(jq '.packageDirectories[0].versionNumber=env.pkgVersion' sfdx-project.json) > sfdx-project.json
    git add sfdx-project.json
    git commit -m "Updating versionNumber to the pending release version. [skip ci]"
    git push origin ${BITBUCKET_BRANCH}

    printf '\n*****************\n'
    echo "Updated versionNumber to ${pkgVersion} in sfdx-project.json in staging branch."
    printf '\n******************************************\n\n'
else
    echo "WARNING:  The remote appears to have changes not in this commit.  If this is not a rerun of an old job manual intervention may be required."
fi