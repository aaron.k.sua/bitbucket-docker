#!/bin/bash
set -e
printf '\n***\n'
echo "Finding version of ${packageName} package to install in ${ENVIRONMENT_NAME}"
printf '\n******************************************\n\n'

source getPackageVersion.sh ${packageName}

printf '\n*****\n'
echo "Beginning install of version ${pkgVersion} of ${packageName} package to ${ENVIRONMENT_NAME}"
printf '\n******************************************\n\n'

sfdx force:package:install --package "${subscriberPackageVersionId}" --installationkey ${INSTALLKEY} --targetusername ${USERNAME} --noprompt --apexcompile package --wait 60 --publishwait 30

printf '\n********\n'
echo "Successfully deployed version ${pkgVersion} of ${packageName} package to ${ENVIRONMENT_NAME}"
printf '\n******************************************\n\n'
