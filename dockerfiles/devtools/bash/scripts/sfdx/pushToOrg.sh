#!/bin/bash
set -e
if [[ ${ENVIRONMENT_NAME} = 'CI Scratch Org']]
    echo "...Creating CI Scratch Org"
    sfdx force:org:create -f config/project-scratch-def.json -a CIScratchOrg --setdefaultusername --wait 60
fi

source installPackageDependencies.sh

echo "...Pushing SFDX Project to ${ENVIRONMENT_NAME}"
sfdx force:source:push -u ${USERNAME} -f
echo "Successfully Completed push to ${ENVIRONMENT_NAME}"
