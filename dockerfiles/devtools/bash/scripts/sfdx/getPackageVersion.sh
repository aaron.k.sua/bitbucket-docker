#!/bin/bash
set -e

# Takes an argument of the packageName referenced here as $1

if [[ ${BITBUCKET_BRANCH} == "production" && -z $1 ]]
then
  echo "Obtaining information for promoting package before deploying to production."
  packageVersion=$(jq -r '.packageDirectories[0].versionNumber' ${PWD}/sfdx-project.json)
  echo "packageVersion is: ${packageVersion}"
  currentPackageVersion="$(echo ${packageVersion} | awk -F '[.]' '{ver=$1"."$2"."$3; print ver}')"
  echo "currentPackageVersion is: ${currentPackageVersion}"
  releaseAlias="$(echo ${packageVersion} | awk -F '[.]' '{ver=$1"."$2"."$3"-"$4; print ver}')"
  echo "releaseAlias is: ${releaseAlias}"
  subscriberPackageVersionId=$(eval $(echo "jq -r '.packageAliases[\"${packageName}@${releaseAlias}\"]' ${PWD}/sfdx-project.json"))
  echo "subscriberPacakgeVersionId is: ${subscriberPackageVersionId}"
else
  content=$(sfdx force:package:version:list -v ${DEVHUBUSERNAME} --json --concise -p $1 --orderby MajorVersion,MinorVersion,PatchVersion,BuildNumber | sed 's/[[:space:]]//g')
  packageDetails=( $(echo $content | jq -c '.result[]') )
  packageDetailsCount=${#packageDetails[@]}
  for ((i=0; i<${packageDetailsCount}; i++))
  do
    packageInfo=${packageDetails[$i]}
    packageVersion=$(echo $packageInfo | jq -r '.Version')
    subscriberPackageVersionId=$(echo $packageInfo | jq -r '.SubscriberPackageVersionId')
  done
fi
echo "Package Version:: ${packageVersion}"
echo "Subscriber Package Version Id:: ${subscriberPackageVersionId}"
export pkgVersion=${packageVersion}
