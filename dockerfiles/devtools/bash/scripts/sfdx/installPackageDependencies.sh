#!/bin/bash
echo "...Installing Live Oak Labs package dependencies"
set -e
PACKAGEDETAILS=$(jq -c '.packageDirectories[]' $PWD/sfdx-project.json)
if [[ ${PACKAGEDETAILS} =~ dependencies ]]
then
  echo "Package has dependencies"
  IFS=$'\n'
  DEPENDENCIES=( $(echo ${PACKAGEDETAILS} | jq -c '.dependencies[]') )
fi
if [[ "${DEPENDENCIES[@]}" ]]
then
  for (( i=0; i<${#DEPENDENCIES[@]}; i++ ))
  do
    DEPENDENCY=${DEPENDENCIES[i]}
    dependencyName=$(echo ${DEPENDENCY} | jq -r '.package')
    versionNumber=$(echo ${DEPENDENCY} | jq -r '.versionNumber')
    if [[ "$versionNumber" == "null" ]]
    then
      echo "WARNING The dependency ${dependencyName} has no version."
    else
      echo "...Searching for the latest package version of ${dependencyName} package"
      source getPackageVersion.sh ${dependencyName}
      echo "...Beginning install of version ${packageVersion} of ${dependencyName} package"
      install=$(sfdx force:package:install --package ${subscriberPackageVersionId} --noprompt --targetusername ${USERNAME} --installationkey ${INSTALLKEY} --apexcompile package --wait 60 --publishwait 10)
      echo "Successfully deployed version ${packageVersion} of ${dependencyName} package"
    fi
  done
else
  echo "No dependencies detected"
fi
