#!/bin/bash
ROOT_DIR=${REPO_PATH:-$PWD}
echo "Build the CloudFormation Artifacts from Source"
#NOTE This is not currently in use for pipeline/octopus.  It is useful for understanding template changes.

if [[ -f scripts/manifest ]] 
then 
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Find Method"
    #Find multiple cdk.json files
    export projects=( $(find ** -maxdepth 2 -name cdk.json -not -path "*node_modules/*" | sed -E -e "s/(.*)cdk.json/\.\/\1/g" | sort -u) )
fi
echo ${projects[@]}
set -e

if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    echo "fetching integration"
  
    IS_SHALLOW=$(git rev-parse --is-shallow-repository)
    if [[ ${IS_SHALLOW} == "true" ]]
    then
        echo "unshallowing"
        git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
        git fetch --unshallow
    else
        echo "not shallow"
        git remote set-branches origin '*'
        git fetch origin integration
    fi
    echo "fetch complete"
    git checkout integration
    INTEGRATION_HASH=($(git rev-parse integration))
    git checkout ${BITBUCKET_BRANCH}
fi

echo ${projects[@]}
for (( i=0; i<${#projects[@]}; i++ ))
do
    project=${projects[$i]}
  
    if [[ -n $1 && ${project} != $1 ]]
    then
        echo "Skipping ${project}"
        continue;
    fi

    if [[ ${BITBUCKET_BRANCH} == "staging" ]]
    then
        INT_DIFF=$(git diff --raw HEAD..${INTEGRATION_HASH} | grep -v "${project}" || true)
        if [[ ${INT_DIFF} == "" ]]
        then
            echo "No diff, skipping build"
            continue
        fi
    fi
  
    if [[ -f "${project}/cdk.json" ]]
    then
        echo "Package: ${project}"
        pushd "${project}"
            echo "Build Cloud Formation Template in ${project}"

            mkdir -p build/
            assumeRole npx cdk synth --output build > template.yml
            mv template.yml build/
        popd
    else
        echo "Not a cdk project"
    fi
done