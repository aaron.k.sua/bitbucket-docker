#!/bin/bash
echo "Tests run on static source before the Artifact is built"
if [[ -f scripts/manifest ]] 
then 
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Find Method"
    #Find multiple package.json files
    export projects=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
fi
echo ${projects[@]}
set -e

for (( i=0; i<${#projects[@]}; i++))
do 
    echo "Project: ${projects[$i]}"
    pushd "${projects[$i]}"
        if [[ -f package.json ]]
        then
            npm run lint
            npm run test
        else
            echo "No tests needed for this project"
        fi
    popd
done