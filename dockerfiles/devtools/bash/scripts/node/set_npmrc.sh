#!/bin/bash
if [[ "${ARTIFACTORY_USER}" != "" ]]
then
    echo 'Configuring LOB NPM repository'
    NPMRC=${BITBUCKET_CLONE_DIR:-$HOME}/.npmrc
    UIDPWD=$(echo -n "$ARTIFACTORY_USER:$ARTIFACTORY_PASSWORD" | openssl enc -base64)
    echo _auth = $UIDPWD > $NPMRC
    echo always-auth = true >> $NPMRC
    echo registry = ${ARTIFACTORY_URL}api/npm/npm/ >> $NPMRC
fi