#!/bin/bash
echo "Build the Artifact from Source"
#TODO support more types of manifests
if [[ -f scripts/manifest ]] 
then 
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Find Method"
    #Find multiple package.json files
    export projects=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
fi
echo ${projects[@]}
set -e

if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    echo "fetching integration"
  
    IS_SHALLOW=$(git rev-parse --is-shallow-repository)
    if [[ ${IS_SHALLOW} == "true" ]]
    then
        echo "unshallowing"
        git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
        git fetch --unshallow
    else
        echo "not shallow"
        git remote set-branches origin '*'
        git fetch origin integration
    fi
    echo "fetch complete"
    git checkout integration
    INTEGRATION_HASH=($(git rev-parse integration))
    git checkout ${BITBUCKET_BRANCH}
fi

echo ${projects[@]}
for (( i=0; i<${#projects[@]}; i++ ))
do
    if [[ ${BITBUCKET_BRANCH} == "staging" ]]
    then
        INT_DIFF=$(git diff --raw HEAD..${INTEGRATION_HASH} | grep -v "\(changes\/\|CHANGELOG.md\)" | grep "${project}" || true)
        if [[ ${INT_DIFF} == "" ]]
        then
            echo "No diff, skipping build"
            continue
        fi
    fi

    project=${projects[$i]}
    echo "project: ${project}"
    if [[ -n $1 && ${project} != $1 ]]
    then
        echo "Skipping ${project}"
        continue;
    fi
    pushd "${project}"
        if [[ -z ${CI} ]] #If not running in CI, skip this step
        then
            rm -f *.zip
        fi

        if [[ -f package.json && ! -f cdk.json ]]
        then
            echo "Creating deployable.zip file"
            if [[ -z ${CI} ]]
            then
                npm install
            else
                npm ci
            fi

            npx npm-pack-zip
            mv *.zip deployable.zip
            cp deployable.zip old_package.zip
            echo "deployable.zip file created"
        fi

        if [[ -f template.yml || -f swagger.yml ]]
        then
            echo "WARNING WARNING WARNING template.zip file behavior is DEPRECATED"
            zip -r -X template.zip template.yml swagger.yml playbooks
            echo "template.zip file created"
        fi

        if [[ -d idp ]]
        then
            echo "WARNING WARNING WARNING idp.zip file behavior is DEPRECATED"
            pushd idp
                zip ../idp.zip * */**
            popd
            echo "idp.zip file created"
        fi

        if [[ -d test ]]
        then
            echo "WARNING WARNING WARNING e2e.zip file behavior is DEPRECATED"
            zip -r -X e2e.zip ./package.json ./package-lock.json ./test
            echo "e2e.zip file created"
        fi

        echo "Creating package.zip file"
        zip -r -X package.zip . -x "node_modules/*" -x "*/node_modules/*" -x "*.zip" -x ".git/*"
        echo "package.zip file created"
    popd
done
