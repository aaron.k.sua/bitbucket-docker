#!/bin/bash
echo "Tests run after an Artifact is built"
cd $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u)
echo ${REPO_PATH:-$PWD}