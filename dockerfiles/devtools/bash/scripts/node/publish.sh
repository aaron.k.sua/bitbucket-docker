#!/bin/bash
echo "Publish requires both set_ssh.sh and set_npmrc.sh to be executed as dependencies."
cd $(find ** -maxdepth 1 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u)
echo ${REPO_PATH:-$PWD}
set -e
npm version patch -m \"Bumping patch version [skip CI]\"
git push origin --follow-tags
npm publish --registry \"https://lob.jfrog.io/lob/api/npm/npm/\"