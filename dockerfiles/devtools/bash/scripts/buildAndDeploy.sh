#!/bin/bash
echo "Build and Deploy Project Artifacts"
set -e
while getopts ":hb:p:" opt; do
  case ${opt} in
    h)
      echo "use -b to specify build command"
      echo "use -p to specify project name"
      ;;
    p)
      project=${OPTARG}
      ;;
    b)
      BUILD_COMMAND=${OPTARG}
      ;;
    \?)
      echo "invalid argument"
      exit
      ;;
  esac
done

if [[ -z ${BUILD_COMMAND} ]]
then
    BUILD_COMMAND="buildNPMPack.sh"
    echo "Warning no build command specified defaulting to: ${BUILD_COMMAND}"
fi

${BUILD_COMMAND} ${project}
deploy.sh ${project}