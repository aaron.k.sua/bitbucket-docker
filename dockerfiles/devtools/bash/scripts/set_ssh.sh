#!/bin/sh
set -e
echo $SSH_KEY > $HOME/.ssh/id_rsa.tmp
base64 -d $HOME/.ssh/id_rsa.tmp > $HOME/.ssh/id_rsa
chmod 600 $HOME/.ssh/*
cp -r $HOME/.ssh/ $BITBUCKET_CLONE_DIR/.ssh/
git remote set-url origin git@bitbucket.org:liveoakbank/${BITBUCKET_REPO_SLUG}.git