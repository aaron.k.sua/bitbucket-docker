#!/bin/bash
echo "Promote the Artifact from a lower environment to the current environment"

#TODO support more types of manifests
if [[ -f scripts/manifest ]] 
then 
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Find Method"
    #Find multiple package.json files
    export projects=( $(find ** -maxdepth 2 -name package.json -not -path "*node_modules/*" | sed -E -e "s/(.*)package.json/\.\/\1/g" | sort -u) )
fi
echo ${projects[@]}
set -e


if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    echo "fetching integration"
  
    IS_SHALLOW=$(git rev-parse --is-shallow-repository)
    if [[ ${IS_SHALLOW} == "true" ]]
    then
        echo "unshallowing"
        git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
        git fetch --unshallow
    else
        echo "not shallow"
        git remote set-branches origin '*'
        git fetch origin integration
    fi
    echo "fetch complete"
    git checkout integration
    INTEGRATION_HASH=($(git rev-parse integration))
    git checkout ${BITBUCKET_BRANCH}
fi

echo ${projects[@]}
for (( i=0; i<${#projects[@]}; i++ ))
do
    project=${projects[$i]}
    echo "project: ${project}"
    if [[ -n $1 && ${project} != $1 ]]
    then
        echo "Skipping ${project}"
        continue;
    fi
    if [[ ${BITBUCKET_BRANCH} == "staging" ]]
    then
        INT_DIFF=$(git diff --raw HEAD..${INTEGRATION_HASH} | grep -v "\(changes\/\|CHANGELOG.md\)" | grep "${project}" || true)
        if [[ ${INT_DIFF} != "" ]]
        then
            echo "Diff detected, skipping promote"
            continue
        fi
    fi

    PROJECTNAME=${project}
    if [[ ${project} == "./" ]]
    then
        PROJECTNAME=${BITBUCKET_REPO_SLUG:-$1}
    fi

    if [[ -z $PROJECTNAME ]]; then echo "\$BITBUCKET_REPO_SLUG not detected or Octopus project name not passed in to \$1"; exit 1; fi
  
    echo $REPO_PATH
    if [[ -z $CI ]]; then
        #if not running in pipeline, alert
        echo "Running locally.  Nothing to do here."
        exit 1
    else
        #get version number and use it for version number in pipeline
        CURRENTVERSION=$(semversioner current-version)
        VERSION="${CURRENTVERSION}.${BITBUCKET_BUILD_NUMBER}"
        echo "version = $VERSION"
    fi
    #TODO Add support for deploying to channels for development environment
    pushd ${project}
        promoterelease.sh $PROJECTNAME
    popd
done
