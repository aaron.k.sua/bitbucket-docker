#!/bin/bash
set -e

GIT_VER=$(semversioner current-version)

if [[ -f scripts/manifest ]] 
then 
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Single Project"
    projects=(${BITBUCKET_REPO_SLUG})
fi
echo ${projects[@]}

for (( i=0; i<${#projects[@]}; i++ ))
do
    PROJECTNAME=${projects[$i]}
    if [[ ${PROJECTNAME} == "./" ]]
    then
        PROJECTNAME=${BITBUCKET_REPO_SLUG:-$(git remote get-url origin | sed -e "s/.*\/\(.*\)\.git/\1/g")}
    fi
    if [[ -n $1 && ${PROJECTNAME} != $1 ]]
    then
        echo "Skipping ${PROJECTNAME}"
        continue;
    fi

    echo "Checking Project: ${PROJECTNAME}"

    #TODO consider making this a script and loading into octopus container
    OCTO_VER=$(octo list-latestdeployments \
        --project=${PROJECTNAME} \
        --environment=${BITBUCKET_BRANCH} \
        --server=${OctopusServer} \
        --apiKey=${OctopusAPIKey} \
        | grep Version: \
        | sed -E -e "s/.*([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)\.[[:digit:]]+.*/\1/g")

    echo "Octopus Version: ${OCTO_VER}"
    echo "Git Version: ${GIT_VER}"

    if [[ ${OCTO_VER} != ${GIT_VER} ]]
    then
        echo "Versions do not match!"
        exit 1
    fi
done
