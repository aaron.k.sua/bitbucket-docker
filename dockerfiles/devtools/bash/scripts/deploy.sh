#!/bin/bash
echo "Deploy the Artifact to the proper environment, or publish a common component Artifact for others"

#TODO support more types of manifests
if [[ -f scripts/manifest ]]
then
    source scripts/manifest
    if [[ -n ${projects} ]]
    then
        echo "Using Project Manifest"
    else
        echo "No projects found in Manifest"
        exit 1
    fi
else
    echo "No Project Manifest Loaded, Using Find Method"
    #Find multiple package.json files
    export projects=( $(find ** -maxdepth 2 -name package.zip | sed -E -e "s/(.*)package.zip/\.\/\1/g" | sort -u) )
fi
set -e


if [[ ${BITBUCKET_BRANCH} == "staging" ]]
then
    echo "fetching integration"

    IS_SHALLOW=$(git rev-parse --is-shallow-repository)
    if [[ ${IS_SHALLOW} == "true" ]]
    then
        echo "unshallowing"
        git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
        git fetch --unshallow
    else
        echo "not shallow"
        git remote set-branches origin '*'
        git fetch origin integration
    fi
    echo "fetch complete"
    git checkout integration
    INTEGRATION_HASH=($(git rev-parse integration))
    git checkout ${BITBUCKET_BRANCH}
fi

case ${BITBUCKET_BRANCH} in
  integration)
    ENV='integration'
    ;;
  staging)
    ENV='staging'
    ;;
  production)
    ENV='production'
    ;;
  *)
    ENV='Developer'
    ;;
esac

echo ${projects[@]}
for (( i=0; i<${#projects[@]}; i++ ))
do
    project=${projects[$i]}
    echo "project: ${project}"
    if [[ -n $1 && ${project} != $1 ]]
    then
        echo "Skipping ${project}"
        continue;
    fi
    if [[ ${BITBUCKET_BRANCH} == "staging" ]]
    then
        INT_DIFF=$(git diff --raw HEAD..${INTEGRATION_HASH} | grep -v "\(changes\/\|CHANGELOG.md\)" | grep "${project}" || true)
        if [[ ${INT_DIFF} == "" ]]
        then
            echo "No diff, skipping deploy"
            continue
        fi
    fi

    PROJECTNAME=${project}
    if [[ ${project} == "./" ]]
    then
        PROJECTNAME=${BITBUCKET_REPO_SLUG:-$(git remote get-url origin | sed -e "s/.*\/\(.*\)\.git/\1/g")}
    fi

    if [[ -z $PROJECTNAME ]]; then echo "\$BITBUCKET_REPO_SLUG not detected or Octopus project name not passed in to \$1"; exit 1; fi

    echo "Proceeding with deployment using Octopus for Project ${PROJECTNAME}"

    if [[ ${ENV} == "Developer" ]]; then
        #if not running in pipeline, increment [patch] version number without adding any changes to the repo
        #TODO add functionality to semversioner to get next version
        NEXTVERSION=$(semversioner current-version | awk -F '[.]' '{ver=$1"."($2 + 1)"."$3; print ver}')
        NEXTBUILD=$(date +%s)
        VERSION="$NEXTVERSION.${NEXTBUILD}-dev"
        echo "version = $VERSION"
    else
        #get version number and use it for version number in pipeline
        CURRENTVERSION=$(semversioner current-version)
        VERSION="${CURRENTVERSION}.${BITBUCKET_BUILD_NUMBER}"
        echo "version = $VERSION"
    fi
    pushd ${project}
        #TODO Add support for deploying to channels for development environment
        createANDdeployrelease.sh $PROJECTNAME $VERSION
    popd
done
