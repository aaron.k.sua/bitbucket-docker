#!/bin/bash
echo "WARNING WARNING WARNING  **************DEPRECATED*******************"
echo "For use in migrating legacy implementation only"
echo "WARNING WARNING WARNING  **************DEPRECATED*******************"

echo "Build a New Artifact or Promote existing Artifact"
BUILD_COMMAND="buildNPMPack.sh"
if [[ -z $1 ]]
then
  echo "WARNING no build command specified, defaulting to ${BUILD_COMMAND}"
else
  BUILD_COMMAND=$1
fi

set -e
#use git commands to determine if commit is a merge coming from the Integration branch
git fetch origin integration
INT_DIFF=$(git diff --raw HEAD..FETCH_HEAD | grep -v "\(changes\/\|CHANGELOG.md\)" || true)
if [[ ${INT_DIFF} == "" ]]
then
  echo "No diff, skipping build to promote Integration to Staging"
else
  echo "Diff detected, build a new Artifact"
  ${BUILD_COMMAND}
  postbuild_test.sh
fi