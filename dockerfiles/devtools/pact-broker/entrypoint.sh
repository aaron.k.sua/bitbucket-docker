#!/bin/sh
if [[ $1 == "--version" ]]
then                                     
  gem search ^pact_broker$               
else                                     
  bundle exec puma --port $PACT_BROKER_PORT  
fi